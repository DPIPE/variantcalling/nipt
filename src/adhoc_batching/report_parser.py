import csv
import re


def get_sample_count(report):
    """get the number of samples in a report
    """

    count = 0

    with open(report) as f:
        for l in f:
            # exclude blank lines and comment lines
            if l.strip() and not l.strip().startswith('#'):
                count += 1

    # exclude header row
    count -= 1

    return count


def get_pool(well, total_samples):
    """
    get pool given well position following:
        1	2	3	4	5	6	7	8	9	10	11	12
     A	A	B	A	B	A	B	A	B	A	B	A	B
     B	B	A	B	A	B	A	B	A	B	A	B	A
     C	A	B	A	B	A	B	A	B	A	B	A	B
     D	B	A	B	A	B	A	B	A	B	A	B	A
     E	A	B	A	B	A	B	A	B	A	B	A	B
     F	B	A	B	A	B	A	B	A	B	A	B	A
     G	A	B	A	B	A	B	A	B	A	B	A	B
     H	B	A	B	A	B	A	B	A	B	A	B	A
    """

    pool = 'A'

    if total_samples < 96:
        return pool

    parsed_well = re.match('(?P<row>[A-H])(?P<column>\d+)', well)
    if parsed_well:
        row = parsed_well.group('row')
        row = ord(row.upper()) - 64
        column = int(parsed_well.group('column'))
        if (row + column) % 2 == 0:
            pool = 'A'
        else:
            pool = 'B'

    return pool


def parse_report(report):
    """parse a adhoc batch initiation report

    @param report: a ad hoc batch initiation report
    @type  report: string(path)

    @return: a tuple of two lists
    @rtype : tuple

    """
    samples = {}
    controls = {}

    num_samples = get_sample_count(report)

    with open(report) as r:
        reader = csv.DictReader(r, delimiter='\t')
        for row in reader:
            batch_name = row['batch_name']
            barcode = row['sample_barcode']
            well = row['well']
            pool = get_pool(well, num_samples)
            if barcode.startswith(batch_name + '_NTC_'):
                controls[barcode] = pool
            else:
                samples[barcode] = pool

    return batch_name, samples, controls


if __name__ == '__main__':
    import sys
    import pprint
    batch, samples, controls = parse_report(sys.argv[1])
    print('batch_name: ', batch)
    print('samples:')
    pprint.pprint(samples)
    print('controls:')
    pprint.pprint(controls)
