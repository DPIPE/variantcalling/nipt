"""
Monitor for new report files matching the given pattern in VeriSeq output dir
"""

import argparse
import os
import re
import sys
import logging

logger = logging.getLogger(__name__)

# diagnostics batches/projects name startswith 'Diag-NIPT'; distinguish with test/training batches
DIAG_BATCH_PREFIX = 'DIAG-NIPT'
# sub-folders in veriseq_output_dir to be checked for reports
SUBDIRS_TO_CHECK = ['ProcessLogs']


class reportWatcher(object):
    def __init__(self, report_repo, report_match, processed_reports_file):
        """ watch for new reports

        @param report_repo: path to the veriseq output folder
        @type  report_repo: string

        @param processed_reports_file: a file containing old/processed reports
        @type  processed_reports_file: string

        """

        self.report_repo = report_repo
        self.report_match = re.compile(report_match)
        self.processed_reports_file = processed_reports_file

    def load_old_reports(self):
        "load old reports to a list"
        old_reports = []
        with open(self.processed_reports_file) as f:
            for l in f:
                old_reports.append(l.strip())

        return old_reports

    def update_old_reports(self, report):
        "append a processed report to old reports file"
        with open(self.processed_reports_file, 'a') as f:
            f.write(report + "\n")

    def watch(self):
        """watch for new reports

        @param param:  Description
        @type  param:  Type

        @return:  Description
        @rtype :  Type

        @raise e:  Description
        """

        old_reports = self.load_old_reports()

        new_reports = []

        for p, dirnames, fs in os.walk(self.report_repo):
            # ignore training/testing batches
            dirnames[:] = [d for d in dirnames
                           if d.upper().startswith(DIAG_BATCH_PREFIX) or d in SUBDIRS_TO_CHECK]
            for f in fs:
                if self.report_match.match(f):
                    fullpath = os.path.join(p, f)
                    if fullpath not in old_reports:
                        new_reports.append(fullpath)
                    else:
                        logger.debug('Skip already processed report: %s', fullpath)

        return new_reports


def main():
    parser = argparse.ArgumentParser(description="Watch for new veriseq reports")
    parser.add_argument("--veriseq-output-dir",
                        dest="veriseq_output_dir",
                        required=True,
                        help="Path to output directory of VeriSeq.")
    parser.add_argument("--report-match",
                        dest="report_match",
                        required=True,
                        help="regex pattern to match the report filename;"
                        " e.g.'.*_batch_initiation_report_.*' ")
    parser.add_argument("--processed-reports-file",
                        dest="processed_reports_file",
                        required=True,
                        help="Path to file containing processed reports.")

    args = parser.parse_args()

    veriseq_output_dir = args.veriseq_output_dir
    processed_reports_file = args.processed_reports_file
    report_match = args.report_match

    watcher = reportWatcher(veriseq_output_dir, report_match, processed_reports_file)
    new_reports = watcher.watch()
    print(new_reports)


if __name__ == "__main__":
    sys.exit(main())
