#!/usr/bin/env python
"""
start a new Step with given artifacts
advance the step untill step finish
"""

import os
import time
import requests
import logging
from genologics.lims import *
from genologics.entities import Process, Queue, Step, ProtocolStep
from genologics.config import USERNAME, PASSWORD, BASEURI

DEBUG = bool("NIPT_DEBUG" in os.environ)

PROJTYPE = "NIPT"

CONTAINER_PREFIX = 'NIPT_'

logger = logging.getLogger(__name__)

if DEBUG:
    logger.setLevel(logging.DEBUG)
    # disable urllib3 InsecureRequestWarning logs
    # ignore urllib3 DEBUG logs
    from urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.WARNING)


class sampleMover(object):
    def __init__(self, queue_id, batch_name, samples, controls=None):
        """
        Create a step with samples and optionally the controls of the given queue;
        advance and finish the step

        @param queue_id:  queue id of target Queue/Process
        @type  queue_id:  string

        @param batch_name:  step UDF "Batch ID" text
        @type  batch_name:  string

        @param samples:  artifacts/samples to be advanced and the sequencing pool
        @type  samples:  dict (key: barcode; value: pool)

        @param controls: controls to be added
        @type  controls: dict (key: barcode; value: pool)

        @return:  None

        """
        self.queue_id = queue_id
        self.batch_name = batch_name
        self.samples = samples
        self.controls = controls
        self._step_artifacts = None
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        self.queue = Queue(self.lims, id=self.queue_id)
        self.max_try = 20

    def get_step_permitted_controls(self):
        """
        get permitted control types of the queue(protocol step)

        @return: list of dict's:
            [{'locked':['false','true'],
              'name': 'some_name',
              'uri': 'some_uri'},]

        """

        protocol_step = ProtocolStep(self.lims, uri=self.queue.protocol_step_uri)

        return protocol_step.permitted_control_types

    def add_controls(self):
        "add NTC(s) to step artifacts"

        num_controls = len(self.controls)
        if num_controls > 4:
            raise RuntimeError("Can not have more than 4 NTCs in a batch.")

        permitted_controls = self.get_step_permitted_controls()

        ntcs = [ct['uri'] for ct in permitted_controls[0:num_controls]]

        return ntcs

    @property
    def step_artifacts(self):
        """
        Prepare step artifacts
        At the same time:
        - add artifact UDF "NIPT Pool": [A, B]
        - add artifact's project UDF "Project type": "NIPT"
        """
        if not self._step_artifacts:
            matching_arts = []
            for art in self.queue.artifacts:
                # artifact name (inheriting submitted sample name) match barcode in ad hoc batch
                # initiation report
                # project name must match batch name
                if art.name in self.samples and art.samples[0].project.name == self.batch_name:
                    matching_arts.append(art)

                    # remove CONTAINER_PREFIX from container name if any
                    container = art.container
                    if container.name.startswith(CONTAINER_PREFIX):
                        prefixed_name = container.name
                        original_name = prefixed_name.lstrip(CONTAINER_PREFIX)
                        container.name = original_name
                        container.put()
                        time.sleep(0.1)
                        logger.info("restored container name from %s to %s",
                                    prefixed_name,
                                    original_name)

                    # update artifact UDF "NIPT Pool"
                    art.udf['NIPT Pool'] = self.samples[art.name]
                    art.put()

                    # update artifact's project UDF "Project type"
                    proj = art.samples[0].project
                    if not proj.udf.get('Project type'):
                        proj.udf['Project type'] = PROJTYPE
                        proj.put()
                        time.sleep(1)
                        proj.get(force=True)  # update existing Project object

            if not matching_arts:
                raise RuntimeError('no sample exists in the queue')

            # checking all samples in ad hoc batch initiation report in queue
            missing_samples_in_queue = []
            for s in self.samples:
                for a in matching_arts:
                    if a.name == s:
                        break
                else:
                    missing_samples_in_queue.append(s)
            if missing_samples_in_queue:
                raise RuntimeError('{} not in "Generate Batch & Sample Sheet" queue'
                                   ''.format(', '.join(missing_samples_in_queue)))

            # add NTCs if there are NTCs in the original ad hoc batch initiation report
            ntcs = self.add_controls() if self.controls else []

            self._step_artifacts = matching_arts + ntcs

            # validation number of artifacts is 24, 48 or 96 (including NTCs)
            if len(self._step_artifacts) not in [24, 48, 96]:
                logger.debug('\n'.join(['samples:'] + [a.name for a in matching_arts]))
                logger.debug('\n'.join(['controls:'] + ntcs))
                raise RuntimeError(
                    "{} samples and {} controls, but requires 24, 48 or 96 in total."
                    "".format(len(matching_arts), len(ntcs)))

        return self._step_artifacts

    def advance(self):
        "Create a step with samples(add controls if any); advance and complete the step"

        all_artifacts = self.step_artifacts

        step = Step.create(self.lims, self.queue.protocol_step_uri, all_artifacts)
        # right after step creation, step.current_state is 'Started';
        # sleep and then force get to update step.current_state so it is 'Record Details'
        time.sleep(15)
        step.get(force=True)

        # trigger Generate Input Sample Sheet
        program_triggered = False
        TRYS = 0
        while step.current_state.upper() != 'COMPLETED':
            if TRYS > self.max_try:
                raise RuntimeError("Failed. Exceeded max tries.")

            TRYS += 1

            if step.program_status:
                # force get program_status
                try:
                    step.program_status.get(
                        force=True
                    )  # http.client.RemotDisconnected > urllib3.exceptions.ProtocolError > requests.exceptions.ConnectionError
                    logger.debug(
                        'Try %s : step.program_status.get(force=True) ok',
                        TRYS)
                except requests.exceptions.ConnectionError:
                    logger.debug(
                        'Try %s : step.program_status.get(force=True) failed, sleep and try later',
                        TRYS)
                    time.sleep(5)
                    continue
                # wait if program is running or queued
                if step.program_status.status in ['QUEUED', 'RUNNING']:
                    logger.debug(
                        'Try %s : step.program_status.status is %s; sleep and try later',
                        TRYS, step.program_status.status)
                    time.sleep(5)
                    continue
                elif step.program_status.status != "OK":
                    logger.warning('step.program_status.status is %s',
                                   step.program_status.status)
                    raise RuntimeError("program failed: {}".format(
                        step.program_status.message))
                else:
                    step.get(force=True)
                    logger.debug(
                        'Try %s : step.program_status.status is %s, step.current_state is "%s", moving forward ...',
                        TRYS, step.program_status.status, step.current_state)
                    # Assign Next Steps finish
                    if step.current_state.upper() == 'COMPLETED':
                        continue
            # update step UDF and then trigger external program at RECORD DETAILS
            if step.current_state.upper() == 'RECORD DETAILS' and not program_triggered:
                logger.debug('Try %s : Updating process UDF(s) ...', TRYS)
                # update step UDF
                proc = Process(self.lims, id=step.id)
                proc.udf['Batch ID'] = self.batch_name
                proc.put()
                time.sleep(10)  # must wait a bit, otherwise trigger external program fails
                # trigger external program
                logger.debug('Try %s : Triggering program ...', TRYS)
                for prog in step.available_programs:
                    if prog.name == 'Check Batch & Generate Sample Sheet':
                        prog.trigger()
                        logger.debug('Try %s : Triggered "%s"', TRYS, prog.name)
                        time.sleep(2)
                        program_triggered = True
                        step.get(force=True)
                        break

                continue

            if DEBUG:  # stop at Assign Next Steps so step can be aborted easily
                try:
                    step.get(force=True)
                except Exception:
                    time.sleep(2)
                    continue
                if step.current_state.upper() == 'ASSIGN NEXT STEPS':
                    logger.debug(
                        'Try %s : Reached "Assign Next Steps" state, stop here',
                        TRYS)
                    return

            try:
                step.get(force=True)
                logger.debug('Try %s : try advancing step from "%s"',
                             TRYS, step.current_state)
                step.advance()
                logger.debug('Try %s : Completing advancing step from %s ...',
                             TRYS, step.current_state)
            except requests.exceptions.HTTPError:
                logger.debug(
                    'Try %s : advancing step from (%s) faild, try again later',
                    TRYS, step.current_state)
                time.sleep(2)
                continue

            time.sleep(10)  # let advancing finish
            step.get(force=True)
        else:
            logger.info('Completed step successfully at try %s!', TRYS)
