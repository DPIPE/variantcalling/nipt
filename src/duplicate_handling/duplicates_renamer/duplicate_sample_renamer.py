#!/usr/bin/env python
"""
given a new nipt report, before sending it to VeriSeq NIPT automation to complete step 3,
check if there are any container duplicates, if yes, rename those containers

check current batch containers, if have 'NIPT_' prefix, remove it so as to match nipt report
"""

import os
import csv
import time
import logging
import requests
from genologics.lims import *
from genologics.entities import Container as gContainer, Artifact as gArtifact
from genologics.config import USERNAME, PASSWORD, BASEURI

import genologics_sql.utils
from genologics_sql.tables import *
from genologics_sql.queries import *

session = genologics_sql.utils.get_session()

DEBUG = bool("NIPT_DEBUG" in os.environ)

CONTAINER_PREFIX = 'NIPT_'

logger = logging.getLogger(__name__)

if DEBUG:
    logger.setLevel(logging.DEBUG)
    # disable urllib3 InsecureRequestWarning logs
    # ignore urllib3 DEBUG logs
    from urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.WARNING)


class duplicateRenamer(object):
    def __init__(self, nipt_report):
        """
        for each sample in a nipt report, rename duplicate containers if any
        :nipt_report: path to a nipt_report (str)
        """
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        self.nipt_report = nipt_report
        self._batch_name = None
        self._report_barcodes = None

    @property
    def batch_name(self):
        if not self._batch_name:
            with open(self.nipt_report) as f:
                # skip comment lines
                reader = csv.DictReader(filter(lambda r: r.lstrip()[0] != '#', f), delimiter='\t')
                for sample_row in reader:
                    batch_in_report = sample_row['batch_name']
                    if batch_in_report:
                        self._batch_name = batch_in_report
                        break

        return self._batch_name

    @property
    def report_barcodes(self):
        if not self._report_barcodes:
            samples = []
            with open(self.nipt_report) as f:
                # skip comment lines
                reader = csv.DictReader(filter(lambda r: r.lstrip()[0] != '#', f), delimiter='\t')
                for sample_row in reader:
                    barcode = sample_row['sample_barcode']
                    if 'NTC' not in barcode:
                        samples.append(barcode)
                self._report_barcodes = samples

        return self._report_barcodes

    def _get_duplicates(self, report_barcode):
        """
        check if there are other containers match the barcode in nipt report
        a barcode in report never have 'NIPT_' prefix
        """

        duplicates = []

        matched_containers_original = query_containers_by_name(session, report_barcode)
        prefixed_barcode = CONTAINER_PREFIX + report_barcode
        matched_containers_prefixed = query_containers_by_name(session, prefixed_barcode)

        matched_containers = matched_containers_original + matched_containers_prefixed

        if not matched_containers:
            raise RuntimeError(
                "Found 0 containers with name {},"
                " but at least one should have been found.".format(report_barcode)
            )

        if len(matched_containers) > 1:
            logger.warning("found %s containers with name %s: %s",
                           len(matched_containers),
                           report_barcode,
                           matched_containers)

        # unique == not duplicate
        if len(matched_containers) == 1:
            return []

        for match in matched_containers:
            matched_container_id = match[1]
            matched_art_id = match[2]

            # empty containers should be renamed regardless
            if not matched_art_id:
                duplicates.append(match)
                continue

            # skip current ( IMPORTANT: batch_name in report must match sample's project name)
            # rather, restore current container name if has prefix to match nipt report barcode
            matched_art = gArtifact(self.lims, id=matched_art_id)
            if matched_art.samples[0].project.name == self.batch_name:

                # remove current container prefix if any
                matched_container = gContainer(self.lims, id=matched_container_id)
                matched_container_name = matched_container.name
                if matched_container_name.startswith(CONTAINER_PREFIX):
                    matched_container.name = matched_container_name.lstrip(CONTAINER_PREFIX)
                    matched_container.put()
                    logger.info("restored %s:%s of batch %s",
                                matched_container,
                                matched_container_name,
                                self.batch_name)

                continue

            duplicates.append(match)

        return duplicates

    def rename_duplicates(self):
        """
        if there are container duplicates, rename those duplicate containers
        """

        for barcode in self.report_barcodes:
            duplicate_containers = self._get_duplicates(barcode)

            if duplicate_containers:
                for dup in duplicate_containers:
                    matched_container_id = dup[1]
                    matched_art_id = dup[2]

                    matched_container = gContainer(self.lims, id=matched_container_id)
                    matched_container_name = matched_container.name

                    # if already has 'NIPT_' prefix, do nothing
                    if matched_container_name.startswith(CONTAINER_PREFIX):
                        continue

                    new_name = CONTAINER_PREFIX + matched_container_name
                    matched_container.name = new_name
                    matched_container.put()

                    # empty containers
                    if not matched_art_id:
                        logger.info("renamed EMPTY container %s:%s to %s",
                                    matched_container,
                                    matched_container_name,
                                    new_name)
                    # non-empty container, log more info
                    else:
                        matched_art = gArtifact(self.lims, id=matched_art_id)
                        matched_sample = matched_art.samples[0]
                        matched_project = matched_sample.project
                        logger.info("renamed %s:%s holding %s:%s of %s:%s from %s to %s",
                                    matched_container,
                                    matched_container_name,
                                    matched_sample,
                                    matched_sample.name,
                                    matched_project,
                                    matched_project.name,
                                    matched_container_name,
                                    new_name)
        # safegarding
        time.sleep(5)
