#!/usr/bin/env python
"""
delete duplicated nipt samples in Generate Batch & Sample Sheet Queue
  type-1-duplicate: already finished workflow in a previous project/run/batch
  type-2-duplicate: new sample included in a previous project but did not get included in that
                    run/batch so there is a newer sample active in workflow
"""

import os
import re
import time
import logging
import requests
from dateutil import parser as dtparser
from genologics.lims import *
from genologics.entities import Queue, Artifact as gArtifact, Workflow
from genologics.config import USERNAME, PASSWORD, BASEURI

import genologics_sql.utils
from genologics_sql.tables import *
from genologics_sql.queries import *

session = genologics_sql.utils.get_session()

DEBUG = bool("NIPT_DEBUG" in os.environ)

NIPT_PROJECT_MATCHER = re.compile(r'Diag-NIPT(?P<project_number>\d+)(?P<minor>[^-]*)-', re.I)

STATUS_ACTIVE = ['QUEUED', 'IN_PROGRESS']

STATUS_COMPLETED = 'COMPLETE'

CONTAINER_PREFIX = 'NIPT_'

logger = logging.getLogger(__name__)

if DEBUG:
    logger.setLevel(logging.DEBUG)
    # disable urllib3 InsecureRequestWarning logs
    # ignore urllib3 DEBUG logs
    from urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.WARNING)


class duplicateDeleter(object):
    def __init__(self, queue_id, workflow_id):
        """
        detect and handle samples in queue_id whose container name have been used before
        """
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        self.queue = Queue(self.lims, id=queue_id)
        self.workflow = Workflow(self.lims, id=workflow_id)

    def _get_matched_containers(self, current_container):
        """
        query db to get containers with the same name as current_container
        """
        original_container_name = current_container.name.lstrip(CONTAINER_PREFIX)
        renamed_container_name = CONTAINER_PREFIX + original_container_name
        _matched_original = query_containers_by_name(session, original_container_name)
        _matched_renamed = query_containers_by_name(session, renamed_container_name)
        matched_containers = _matched_original + _matched_renamed

        if not matched_containers:
            logger.error(
                "Found 0 containers with name {} or {},"
                " but at least one({}) should have been found.".format(
                    original_container_name,
                    renamed_container_name,
                    current_container))
        elif len(matched_containers) > 1:
            logger.debug("found %s containers matching name %s(or %s): %s",
                         len(matched_containers),
                         original_container_name,
                         renamed_container_name,
                         matched_containers)
        else:
            logger.debug("unique container name %s", current_container.name)

        return matched_containers

    def _check_type1_duplicate(self, current_art, matched_containers):
        """
        check current sample is a type 1 duplicate
        type 1 duplicate:
        - 1A: finished workflow and passed qc in a previous project/run/batch
        - 1B: a finished workflow and passed qc sample get re-queued or re-added to nipt workflow
        """
        is_type1_duplicate = False

        current_sample = current_art.samples[0]
        current_project = current_sample.project
        current_project_name = current_sample.project.name
        current_container = current_art.container

        for match in matched_containers:
            matched_container_id = match[1]
            matched_art_id = match[2]

            # skip checking empty matched container
            if not matched_art_id:
                continue

            matched_art = gArtifact(self.lims, id=matched_art_id)
            matched_sample = matched_art.samples[0]
            m_sample_name = matched_sample.name
            m_sample_proj_name = matched_sample.project.name

            for stage_status in matched_art.workflow_stages_and_statuses:  # empty list in queue
                status = stage_status[1]
                step_name = stage_status[2]

                workflow_steps = self.workflow.stages
                _final_step = workflow_steps[-1].name

                # already finished workflow
                if step_name == _final_step and status == STATUS_COMPLETED:
                    # type 1A duplicate - delete
                    if matched_art.udf['qc_flag'] == 'PASS':
                        if matched_container_id != current_container.id:
                            is_type1_duplicate = '1A'
                            logger.info('%s:%s of %s:%s is a TYPE %s DUPLICATE (%s:%s of %s'
                                        ' finished NIPT workflow and passed qc already)',
                                        current_sample,
                                        current_sample.name,
                                        current_project,
                                        current_project_name,
                                        is_type1_duplicate,
                                        matched_sample,
                                        m_sample_name,
                                        m_sample_proj_name)
                        else:
                            # type 1B duplicate: a completed nipt workflow and passed qc sample was
                            # "re-queued" or "added to nipt workflow again". Then no need to try to
                            # delete it later. Since that work has been done on it, not possible to
                            # delete anyways)
                            is_type1_duplicate = '1B'
                            logger.info('%s:%s of %s:%s is a TYPE %s DUPLICATE (itself finished'
                                        ' NIPT workflow and passed QC before)',
                                        current_sample,
                                        current_sample.name,
                                        current_project,
                                        current_project_name,
                                        is_type1_duplicate)
                        break
            else:
                continue
            break

        return is_type1_duplicate

    def _check_type2_duplicate(self, current_art, matched_containers):
        """
        check current sample is a type 2 duplicate
        type 2 duplicate: included in a previous project but did not get included in the run/batch
                          so there is a newer sample active in workflow
        - 2A: newer sample receive date
        - 2B: same sample receive date, but project # has a suffix(thus possibly a newer project)
        """
        is_type2_duplicate = False

        current_sample = current_art.samples[0]
        current_project = current_sample.project
        current_project_name = current_project.name
        current_container = current_art.container

        for match in matched_containers:
            matched_container_id = match[1]
            matched_art_id = match[2]

            # skip checking empty matched container
            if not matched_art_id:
                continue

            # skip current
            # doesn't make sense to compare date_received to itself
            if matched_container_id == current_container.id:
                continue

            matched_art = gArtifact(self.lims, id=matched_art_id)
            matched_sample = matched_art.samples[0]
            m_sample_name = matched_sample.name
            m_sample_proj_name = matched_sample.project.name

            # check matched art is active(QUEUED or IN_PROGRESS) in workflow
            # and is newer than current sample
            # if there is a newer sample but not active in nipt workflow, current sample is kept
            # and marked as duplicate only until this newer one or another newer one is active in
            # workflow
            for stage_status in matched_art.workflow_stages_and_statuses:
                status = stage_status[1]
                step_name = stage_status[2]

                workflow_steps = self.workflow.stages

                # was a new sample in previous project but was not run in that batch/project
                # compare sample receive date, if matched sample is newer then current sample,
                # then current sample is
                # a type 2 duplicate
                # if recieve dates are the same, compare project number
                # if project numbers are the same, newer project has a suffix, e.g. NIPTB or NIPT_B
                for stage in workflow_steps:
                    if step_name == stage.name and status in STATUS_ACTIVE:
                        cdate = dtparser.parse(current_sample.date_received)
                        mdate = dtparser.parse(matched_sample.date_received)
                        if mdate > cdate:
                            is_type2_duplicate = '2A'
                            logger.info('%s:%s of %s:%s is a TYPE %s DUPLICATE(%s:%s of %s is'
                                        ' a newer sample)',
                                        current_sample,
                                        current_sample.name,
                                        current_project,
                                        current_project_name,
                                        is_type2_duplicate,
                                        matched_sample,
                                        m_sample_name,
                                        m_sample_proj_name)
                            break
                        # same date(multiple projects created same date, compare project number)
                        elif mdate == cdate:
                            m_proj_name = matched_sample.project.name
                            c_proj_name = current_sample.project.name
                            m_proj_match = NIPT_PROJECT_MATCHER.match(m_proj_name).groupdict()
                            c_proj_match = NIPT_PROJECT_MATCHER.match(c_proj_name).groupdict()
                            if m_proj_match and c_proj_match:
                                m_proj_number = int(m_proj_match['project_number'])
                                m_proj_minor = m_proj_match['minor']
                                c_proj_number = int(c_proj_match['project_number'])
                                c_proj_minor = c_proj_match['minor']
                                if (m_proj_number > c_proj_number
                                        or (m_proj_number == c_proj_number
                                            and m_proj_minor != ''  # e.g. NIPT10B or NIPT10_B
                                            and c_proj_minor == '')):
                                    is_type2_duplicate = '2B'
                                    logger.info('%s:%s of %s:%s is a TYPE %s DUPLICATE(%s:%s of'
                                                ' %s is in a newer project)',
                                                current_sample,
                                                current_sample.name,
                                                current_project,
                                                current_project_name,
                                                is_type2_duplicate,
                                                matched_sample,
                                                m_sample_name,
                                                m_sample_proj_name)
                                    break
                else:
                    continue
                break
            else:
                continue
            break

        return is_type2_duplicate

    def _is_duplicate(self, current_art):
        """
        check if a container has duplicate
        :return: ['True', '1A', '1B', False]
        """

        is_duplicate = False

        current_container = current_art.container

        matched_containers = self._get_matched_containers(current_container)

        type1_dup = self._check_type1_duplicate(current_art, matched_containers)

        if type1_dup:  # [False, '1A', '1B']
            is_duplicate = type1_dup  # '1A' duplicate can't be deleted
        elif self._check_type2_duplicate(current_art, matched_containers):  # [False, '2A', '2B']
            is_duplicate = True  # always delete

        return is_duplicate

    def delete_duplicates(self):
        """
        check if the container name of each artifact in queue has been registerd before, if yes:
            if a previous sample finished the workflow and passed qc (type 1 duplicate):
                delete current sample in queue
            elif a newer sample is active in nipt workflow (type 2 duplicate):
                delete current sample in queue
            else
                do nothing
        """

        for current_art in self.queue.artifacts:

            is_duplicate = self._is_duplicate(current_art)

            current_sample = current_art.samples[0]
            current_sample_name = current_sample.name
            current_container = current_art.container
            current_project = current_sample.project
            current_project_name = current_project.name

            if is_duplicate:  # ['1A', '1B', '2A', '2B']
                if is_duplicate == '1B':
                    logger.warning('can not delete a TYPE 1B DUPLICATE: %s:%s of %s:%s',
                                   current_sample,
                                   current_sample_name,
                                   current_project,
                                   current_project_name)
                    continue

                # delete other types of duplicates
                # delete sample first
                try:
                    self.lims.delete(current_sample.uri)
                    logger.info("deleted %s:%s of %s:%s",
                                current_sample,
                                current_sample_name,
                                current_project,
                                current_project_name)
                    time.sleep(1)
                except requests.exceptions.HTTPError as err:
                    logger.error(getattr(err, 'message', err))

                # then delete container
                try:
                    self.lims.delete(current_container.uri)
                    logger.info("deleted %s:%s holding %s:%s of %s:%s",
                                current_container,
                                current_container.name,
                                current_sample,
                                current_sample_name,
                                current_project,
                                current_project_name)
                except requests.exceptions.HTTPError as err:
                    logger.error(getattr(err, 'message', err))
            else:
                logger.debug('unique %s holding %s:%s of %s:%s',
                             current_container,
                             current_sample,
                             current_sample_name,
                             current_project,
                             current_project_name)
