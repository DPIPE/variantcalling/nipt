#!/usr/bin/env python
"""
detect duplicated samples(containers) which are pure duplicates that need to be deleted
"""
import os
import sys
import datetime
import argparse
import logging

DEBUG = bool("NIPT_DEBUG" in os.environ)

logFormatter = logging.Formatter("%(asctime)-25s %(name)-30s %(levelname)-10s %(message)s")
logger = logging.getLogger()

logfileDir = os.path.join(sys.path[0], *(['..'] * 4 + ['logs'] + ['duplicates_deleter']))
try:
    os.makedirs(logfileDir)
except OSError:
    pass

if DEBUG:
    # per run log
    log_file = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
else:
    try:
        # per week log when looping or cronjob
        from dateutil.relativedelta import relativedelta, MO
        delta = relativedelta(weekday=MO(-1))
        this_monday = datetime.date.today() + delta
        log_file = this_monday.strftime('%Y-%m-%d')
    except Exception:
        # per month log, when looping or cronjob
        log_file = datetime.date.today().replace(day=1).strftime('%Y-%m-%d')

fileHandler = logging.FileHandler("{0}/{1}.log".format(
    logfileDir,
    log_file))

fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.DEBUG)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.DEBUG)
logger.addHandler(consoleHandler)

if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# disalbe requests and urllib3 logs
logging.getLogger('requests').setLevel(logging.ERROR)
logging.getLogger('urllib3').setLevel(logging.ERROR)

logger.debug('start.......')

from duplicate_sample_deleter import duplicateDeleter

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="monitor queue and detect and handle duplicated samples(containers)")

    parser.add_argument("--queue-id",
                        dest="queue_id",
                        required=True,
                        help="Queue from which duplicated samples are monitored")

    parser.add_argument("--workflow-id",
                        dest="workflow_id",
                        required=True,
                        help="NIPT workflow ID")

    args = parser.parse_args()

    queue_id = args.queue_id
    workflow_id = args.workflow_id

    # delete samples with duplicated container name(sample barcode/id) and is a duplicate
    duplicate_deleter = duplicateDeleter(queue_id, workflow_id)
    duplicate_deleter.delete_duplicates()
