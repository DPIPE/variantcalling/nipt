import os
import shutil
import subprocess
import csv
import logging
from genologics.lims import *
from genologics.entities import Queue
from genologics.config import USERNAME, PASSWORD, BASEURI

logger = logging.getLogger(__name__)


class ReportTransporter(object):
    def __init__(self, src, dest_repo):
        """
        copy src file to dest repo
        @param src: a file to be copied to dest repo
        @type  src: string

        @param dest_repo: a directory to copy src to
        @type  dest_repo: sting
        """
        self.src = src
        self.dest_repo = dest_repo

    def transport(self, parent_dirs):
        """
        copy src file to dest dir
        keep parent dirs to some levels

        @param parent_dirs: how many levels of parent dirs of src to be kept in dest
                            0: basename only
                            1: immediate_parent/basename
                            2: 2nd_parent/immediate_parent/basename
        @type  parent_dirs: int
        """
        _parts = self.src.split('/')
        subdirs = '/'.join(_parts[-(parent_dirs + 1):-1])
        dest_dir = os.path.join(self.dest_repo, subdirs)
        try:
            os.makedirs(dest_dir)
        except Exception:
            pass

        # .tab and .tab.md5 files must be copied together
        md5_path = self.src + '.md5'
        if os.path.exists(md5_path):
            shutil.copy(self.src, dest_dir)
            shutil.copy(md5_path, dest_dir)
            return True
        else:
            return False


class PostQueueTransporter(ReportTransporter):
    """
    copy src to dest_repo only when all samples in src are queued
    """
    def __init__(self, src, dest_repo, target_queue_id):
        self.queue_id = target_queue_id
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        self.queue = Queue(self.lims, id=self.queue_id)
        super(PostQueueTransporter, self).__init__(src, dest_repo)

    def all_queued(self):
        """
        check if all samples in "src" report are already in "queue_id" Queue
        batch_name in "src" report matches queue artifact "Batch ID" UDF
        """

        # batch_name and sample in src report
        batch_in_report = ""
        samples_in_report = []
        with open(self.src) as f:
            # skip comment lines
            reader = csv.DictReader(filter(lambda r: r.lstrip()[0] != '#', f),
                                    delimiter='\t')
            for sample_row in reader:
                batch_in_report = sample_row['batch_name']
                barcode = sample_row['sample_barcode']
                samples_in_report.append(barcode)

        # all queued samples matching batch_name
        queued_samples = [art.name for art in self.queue.artifacts if art.udf['Batch ID'] == batch_in_report]

        if not all(s in queued_samples for s in samples_in_report if 'NTC' not in s):
            return False
        else:
            return True

    def transport(self, parent_dirs):
        if self.all_queued():
            return super(PostQueueTransporter, self).transport(parent_dirs)
        else:
            logger.warning("not all samples in report %s are in Queue '%s'(%s)", self.src, self.queue.name, self.queue.id)
            return False


class PostProgressTransporter(ReportTransporter):
    """
    copy src to dest_repo only when all samples is process_type IN PROGRESS
    """
    def __init__(self, src, dest_repo, target_process_type):
        self.target_process_type = target_process_type
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        super(PostProgressTransporter, self).__init__(src, dest_repo)

    def all_in_progress(self):
        """
        check if all samples in "src" report are already in progress of "target_process_type" Process
        """

        # batch_name and sample in src report
        batch_in_report = ""
        samples_in_report = []
        with open(self.src) as f:
            # skip comment lines
            reader = csv.DictReader(filter(lambda r: r.lstrip()[0] != '#', f),
                                    delimiter='\t')
            for sample_row in reader:
                batch_in_report = sample_row['batch_name']
                barcode = sample_row['sample_barcode']
                samples_in_report.append(barcode)

        all_processes = self.lims.get_processes(type=self.target_process_type)

        if not all_processes:
            return False

        # check latest process first
        all_processes.reverse()
        found_any_step = False
        found_target_step = False
        for p in all_processes:
            if p.step.current_state == 'Record Details':
                logger.debug('Found a %s step in "Record Details" status (id: %s)', self.target_process_type, p.id)
                found_any_step = True
                logger.debug('checking if step(%s) includes all samples in %s', p.id, self.src)

                # [({'limsid': 'input1_limsid',},{'uri':Artifact(input1_artifact_uri)}),({},{})]
                process_inputs = []
                for inp, _ in p.input_output_maps:
                    inp_name = inp['uri'].name
                    inp_batch_id = inp['uri'].udf.get('Batch ID')
                    if inp_batch_id == batch_in_report:
                        if inp_name not in process_inputs:
                            process_inputs.append(inp_name)

                # input report samples(do not check NTC) which are not input of step p
                in_report_not_in_step = [
                    report_sample for report_sample in samples_in_report
                    if 'NTC' not in report_sample and report_sample not in process_inputs
                ]

                # all input report samples(do not check NTC) are input of step p
                if not in_report_not_in_step:
                    logger.debug('step %s includes all samples in %s', p.id, self.src)
                    found_target_step = True
                    return True
                else:
                    logger.debug(
                        '%s samples(NTC not included) in %s are not inputs of step %s:\n\t%s',
                        len(in_report_not_in_step),
                        self.src, p.id,
                        '\n\t'.join(in_report_not_in_step))
        if not found_any_step:
            logger.warning(
                'Could not find a "%s" step at "Record Details" status',
                self.target_process_type)
        elif not found_target_step:
            logger.warning(
                'Could not find a "%s" step at "Record Details" status includes all samples in %s',
                self.target_process_type, self.src)

        return False

    def transport(self, parent_dirs):
        if self.all_in_progress():
            return super(PostProgressTransporter, self).transport(parent_dirs)
        else:
            return False
