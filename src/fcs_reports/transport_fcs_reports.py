#!/usr/bin/env python
"""
look for VeriSeq reports and copy it over to the folder monitored by File Capture Service if queue ready
"""
import os
import sys
import datetime
import argparse
import logging

from adhoc_batching.report_watcher import reportWatcher
from report_transporter import ReportTransporter, PostQueueTransporter, PostProgressTransporter
from duplicate_handling.duplicates_renamer.duplicate_sample_renamer import duplicateRenamer

DEBUG = bool("NIPT_DEBUG" in os.environ)

BATCH_INITIATION_REPORT_QUEUE_ID = "2107"
NIPT_REPORT_PROCESS_TYPE = "AUTOMATED - Data Analysis (VeriSeq NIPT v1.0)_EHD"

REPORTS = [
    {
        "pattern": ".*_batch_initiation_report_.*.tab$",
        "type": "Batch Initiation Report",
        "watcher": reportWatcher,
        "transporter": PostQueueTransporter,
        "parent_dirs": 2,
        "queue_id": BATCH_INITIATION_REPORT_QUEUE_ID
    },
    {
        "pattern": ".*_sample_invalidation_report_.*.tab$",
        "type": "Sample Invalidation Report",
        "watcher": reportWatcher,
        "transporter": PostProgressTransporter,
        "parent_dirs": 1,
        "process_type": NIPT_REPORT_PROCESS_TYPE
    },
    {
        "pattern": ".*_sample_cancellation_report_.*.tab$",
        "type": "Sample Cancellation Report",
        "watcher": reportWatcher,
        "transporter": PostProgressTransporter,
        "parent_dirs": 1,
        "process_type": NIPT_REPORT_PROCESS_TYPE
    },
    {
        "pattern": ".*_nipt_report_.*.tab$",
        "type": "NIPT Report",
        "watcher": reportWatcher,
        "transporter": PostProgressTransporter,
        "parent_dirs": 1,
        "process_type": NIPT_REPORT_PROCESS_TYPE
    },
]

logFormatter = logging.Formatter("%(asctime)-25s %(name)-30s %(levelname)-10s %(message)s")
logger = logging.getLogger()

logfileDir = os.path.join(sys.path[0], *(['..'] * 3 + ['logs'] + ['fcs_reports']))
try:
    os.makedirs(logfileDir)
except OSError:
    pass

if DEBUG:
    # per run log
    log_file = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
else:
    try:
        # per week log when looping or cronjob
        from dateutil.relativedelta import relativedelta, MO
        delta = relativedelta(weekday=MO(-1))
        this_monday = datetime.date.today() + delta
        log_file = this_monday.strftime('%Y-%m-%d')
    except Exception:
        # per month log, when looping or cronjob
        log_file = datetime.date.today().replace(day=1).strftime('%Y-%m-%d')

fileHandler = logging.FileHandler("{0}/{1}.log".format(
    logfileDir,
    log_file))

fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.DEBUG)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.DEBUG)
logger.addHandler(consoleHandler)

if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# disalbe requests and urllib3 logs
logging.getLogger('requests').setLevel(logging.ERROR)
logging.getLogger('urllib3').setLevel(logging.ERROR)

logger.debug('start.......')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Watch for new ad hoc veriseq batch initiation reports;"
        " create a new 'Generate Batches & Input Sample Sheet' step for a batch;"
        " generate input sample sheet containing complete metadata;"
        " complete the step.")

    parser.add_argument(
        "--veriseq-output-dir",
        dest="veriseq_output_dir",
        required=True,
        help="Path to output directory of VeriSeq.")

    parser.add_argument(
        "--fcs-monitor-dir",
        dest="fcs_monitor_dir",
        required=True,
        help="Path to batch directory monitored by File Capture Service")

    parser.add_argument(
        "--processed-reports-file",
        dest="processed_reports_file",
        required=True,
        help="Path to file containing processed batch initiation reports.")

    args = parser.parse_args()

    veriseq_output_dir = args.veriseq_output_dir
    processed_reports_file = args.processed_reports_file
    fcs_monitor_dir = args.fcs_monitor_dir

    for report_type in REPORTS:
        watcher = report_type["watcher"](veriseq_output_dir,
                                         report_type["pattern"],
                                         processed_reports_file)
        new_reports = watcher.watch()

        if not new_reports:
            logger.debug('no new report matching "%s"', report_type["pattern"])

        for report in new_reports:
            logger.info('Found a new %s: %s', report_type["type"], report)

            # these report types requires unique container name in Clarity
            # rename duplicated containers
            if report_type['type'] in ["NIPT Report",
                                       "Sample Invalidation Report",
                                       "Sample Cancellation Report"]:
                logger.debug("rename containers if duplicates exist")
                duplicates_renamer = duplicateRenamer(report)
                duplicates_renamer.rename_duplicates()

            try:
                # transport report
                if "queue_id" in report_type:
                    transporter = report_type["transporter"](
                        report, fcs_monitor_dir, report_type["queue_id"])
                elif "process_type" in report_type:
                    transporter = report_type["transporter"](
                        report, fcs_monitor_dir, report_type["process_type"])
                else:
                    transporter = report_type["transporter"](
                        report, fcs_monitor_dir)

                sucess = transporter.transport(report_type["parent_dirs"])

                if sucess:
                    logger.info('Successfully copied %s to %s', report, fcs_monitor_dir)

                    # append to processed reports file
                    watcher.update_old_reports(report)
                else:
                    logger.debug('copy faild from %s to %s', report, fcs_monitor_dir)

            except Exception as e:
                logger.exception(e)
