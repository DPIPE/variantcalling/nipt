import csv

UDF = [
    {
        "process": "EXTRACTION:setup",
        "reagent_name": "Extraction_Kit"
    },
    {
        "process": "LIBRARY:setup",
        "reagent_name": "Library_Kit"
    },
    {
        "process": "QUANT:setup",
        "reagent_name": "Accessory_Kit",
    },
]


def parse_report(report):
    """parse a VeriSeq report

    @param report: a VeriSeq report
    @type  report: string(path)

    @return: batch_name and batch reagent names and lots
    @rtype : tuple of batch_name(string) and dict of reagents

    """

    reagents = dict()

    with open(report) as r:
        reader = csv.DictReader(r, delimiter='\t')
        for row in reader:
            batch_name = row['batch_name']
            for udf in UDF:
                if (udf["process"] == row["process"] and
                        udf["reagent_name"] == row["reagent_name"]):
                    reagents[udf["reagent_name"]] = 'lot: ' + row['lot']

    return batch_name, reagents


if __name__ == '__main__':
    import sys
    import pprint
    reagents = parse_report(sys.argv[1])
    pprint.pprint(reagents)
