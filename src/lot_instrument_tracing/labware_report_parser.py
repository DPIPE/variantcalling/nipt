import csv


def parse_report(report):
    """parse a labware report

    @param report: a labware report
    @type  report: string(path)

    @return: batch_name, labware_name and labware_barcode
    @rtype : (string, dict)

    """

    labwares = dict()

    with open(report) as r:
        reader = csv.DictReader(r, delimiter='\t')
        for row in reader:
            batch_name = row['batch_name']
            labwares[row['labware_name']] = row['labware_barcode']

    return batch_name, labwares


if __name__ == '__main__':
    import sys
    import pprint
    labwares = parse_report(sys.argv[1])
    pprint.pprint(labwares)
