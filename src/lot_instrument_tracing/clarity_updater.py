#!/usr/bin/env python
"""
start a new Step with given artifacts
advance the step untill step finish
"""

import os
import requests
import logging
from genologics.lims import *
from genologics.config import USERNAME, PASSWORD, BASEURI

DEBUG = bool('NIPT_DEBUG' in os.environ)

logger = logging.getLogger(__name__)

if DEBUG:
    logger.setLevel(logging.DEBUG)
    # disable urllib3 InsecureRequestWarning logs
    # ignore urllib3 DEBUG logs
    from urllib3.exceptions import InsecureRequestWarning
    requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.WARNING)

PROCESS_TYPE = 'Generate Batch & Input Sample Sheet (VeriSeq NIPT v1.0)_EHD'


class processUpdater(object):
    def __init__(self, batch_name, udfs):
        """
        find the process whose UDF 'Batch ID' matches batch_name; update process UDF with udfs

        @param batch_name: matches 'Batch ID' UDF of a process
        @type  batch_name: string

        @param udfs:  process UDFs to be added
        @type  udfs:  dict

        @return:  None

        """
        self.udfs = udfs
        self.batch_name = batch_name
        self.lims = Lims(BASEURI, USERNAME, PASSWORD)
        self._target_process = None

    @property
    def target_process(self):
        """
        look for a process whose UDF 'Batch ID' matches self.batch_name
        """

        if not self._target_process:
            proc = self.lims.get_processes(type=PROCESS_TYPE, udf={"Batch ID": self.batch_name})
            if not proc:
                raise RuntimeError(
                    "no process found for {} whose UDF 'Batch ID' is {}".format(
                        PROCESS_TYPE,
                        self.batch_name))
            elif len(proc) > 1:
                raise RuntimeError(
                    "more than one processes found for {} whose UDF 'Batch ID' is {}: {}".format(
                        PROCESS_TYPE,
                        self.batch_name,
                        proc))
            else:
                self._target_process = proc[0]

        return self._target_process

    def update_udfs(self):
        "update process udfs with self.udfs"
        logger.debug('Updating process UDFs ...')
        try:
            for k, v in self.udfs.items():
                    self.target_process.udf[k] = v
                    logger.debug("Updated UDF %s to %s", k, v)
            self.target_process.put()
            logger.info("Finished updating UDFs of %s", self.target_process)
        except Exception:
            logger.exception("Update process UDF failed")
            raise
