#!/usr/bin/env python
"""
parse new initiation report
start a new Step of "Generate Batch & Input Sample Sheet"
trigger "Check Batch & Generate Sample Sheet" to generate a full Input Sample Sheet
complete the Step so the batch is queued in "AUTOMATED - Data Analysis"
"""
import os
import sys
import datetime
import argparse
import logging

from adhoc_batching.report_watcher import reportWatcher
from reagent_report_parser import parse_report as reagent_parser
from labware_report_parser import parse_report as labware_parser
from clarity_updater import processUpdater

DEBUG = bool("NIPT_DEBUG" in os.environ)

logFormatter = logging.Formatter("%(asctime)-25s %(name)-30s %(levelname)-10s %(message)s")
logger = logging.getLogger()

logfileDir = os.path.join(sys.path[0], *(['..'] * 3 + ['logs'] + ['lot_instrument_tracing']))
try:
    os.makedirs(logfileDir)
except OSError:
    pass

if DEBUG:
    # per run log
    log_file = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
else:
    try:
        # per week log when looping or cronjob
        from dateutil.relativedelta import relativedelta, MO
        delta = relativedelta(weekday=MO(-1))
        this_monday = datetime.date.today() + delta
        log_file = this_monday.strftime('%Y-%m-%d')
    except Exception:
        # per month log, when looping or cronjob
        log_file = datetime.date.today().replace(day=1).strftime('%Y-%m-%d')

fileHandler = logging.FileHandler("{0}/{1}.log".format(
    logfileDir,
    log_file))

fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.DEBUG)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.DEBUG)
logger.addHandler(consoleHandler)

if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# disalbe requests and urllib3 logs
logging.getLogger('requests').setLevel(logging.ERROR)
logging.getLogger('urllib3').setLevel(logging.ERROR)

REPORTS = [
    {
        "pattern": ".*_library_reagent_report_.*.tab$",
        "type": "Library Reagent Report",
        "watcher": reportWatcher,
        "parser": reagent_parser,
        "clarity_action": processUpdater
    },
    # {
    #     "pattern": ".*_library_labware_report_.*.tab$",
    #     "watcher": reportWatcher,
    #     "parser": labware_parser,
    #     "clarity_action": processUpdater
    # },
]

logger.debug('start.......')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Watch for new ad hoc veriseq batch initiation reports;"
        " create a new 'Generate Batches & Input Sample Sheet' step for a batch;"
        " generate input sample sheet containing complete metadata;"
        " complete the step.")

    parser.add_argument("--veriseq-output-dir",
                        dest="veriseq_output_dir",
                        required=True,
                        help="Path to output directory of VeriSeq.")

    parser.add_argument(
        "--processed-reports-file",
        dest="processed_reports_file",
        required=True,
        help="Path to file containing processed batch initiation reports.")

    args = parser.parse_args()

    veriseq_output_dir = args.veriseq_output_dir
    processed_reports_file = args.processed_reports_file

    for report_type in REPORTS:
        watcher = report_type["watcher"](veriseq_output_dir,
                                         report_type["pattern"],
                                         processed_reports_file)
        new_reports = watcher.watch()

        if not new_reports:
            logger.debug('no new report matching "%s"', report_type["pattern"])

        for report in new_reports:
            logger.info('Found a new %s: %s', report_type['type'], report)
            try:
                # parser report
                batch, udfs = report_type["parser"](report)

                # clarity actions
                report_type["clarity_action"](batch, udfs).update_udfs()

                # append to processed reports file
                watcher.update_old_reports(report)
            except Exception as e:
                logger.exception(e)
