#!/usr/bin/env python
"""
detect new nipt reports;
restructure and interpret the information;
genereate a transformed report for importing into SWL
"""
import os
import sys
import datetime
import argparse
import csv
import re
import logging

DEBUG = bool("NIPT_DEBUG" in os.environ)

logFormatter = logging.Formatter("%(asctime)-25s %(name)-30s %(levelname)-10s %(message)s")
logger = logging.getLogger()

logfileDir = os.path.join(sys.path[0], *(['..'] * 3 + ['logs'] + ['nipt_report_interpreter']))
try:
    os.makedirs(logfileDir)
except OSError:
    pass

if DEBUG:
    # per run log
    log_file = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
else:
    try:
        # per week log when looping or cronjob
        from dateutil.relativedelta import relativedelta, MO
        delta = relativedelta(weekday=MO(-1))
        this_monday = datetime.date.today() + delta
        log_file = this_monday.strftime('%Y-%m-%d')
    except Exception:
        # per month log, when looping or cronjob
        log_file = datetime.date.today().replace(day=1).strftime('%Y-%m-%d')

fileHandler = logging.FileHandler("{0}/{1}.log".format(
    logfileDir,
    log_file))

fileHandler.setFormatter(logFormatter)
fileHandler.setLevel(logging.DEBUG)
logger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
consoleHandler.setLevel(logging.DEBUG)
logger.addHandler(consoleHandler)

if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

logger.debug('start.......')

from adhoc_batching.report_watcher import reportWatcher
from nipt_report_parser import parse_report


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="detect new nipt reports; transform and interpretinto the"
        " information and genereate a new tsv report for importing into SWL")

    parser.add_argument("--veriseq-output-dir",
                        dest="veriseq_output_dir",
                        required=True,
                        help="Path to the output directory of VeriSeq")

    parser.add_argument("--interpreted-output-dir",
                        dest="interpreted_output_dir",
                        required=True,
                        help="Path to the output directory of interpreted nipt report")

    parser.add_argument("--report-match",
                        dest="report_match",
                        required=True,
                        help="regex pattern to match the nipt report filename, "
                        "e.g.'.*_nipt_report_.*' ")

    parser.add_argument(
        "--processed-reports-file",
        dest="processed_reports_file",
        required=True,
        help="Path to file containing already processed nipt reports.")

    args = parser.parse_args()

    veriseq_output_dir = args.veriseq_output_dir
    interpreted_output_dir = args.interpreted_output_dir
    report_match = args.report_match
    processed_reports_file = args.processed_reports_file

    try:
        os.mkdir(interpreted_output_dir)
    except OSError:
        pass

    watcher = reportWatcher(veriseq_output_dir, report_match, processed_reports_file)

    new_reports = watcher.watch()

    if not new_reports:
        logger.debug('no new report matching "%s"', report_match)

    for report in new_reports:
        logger.info('Found a new NIPT Report: %s', report)
        logger.debug('Processing %s', report)
        try:
            parse_report(report, interpreted_output_dir)
            logger.info('Finished processing nipt report %s successfully', report)

            watcher.update_old_reports(report)
        except Exception as e:
            logger.exception(e)
