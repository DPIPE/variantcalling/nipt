# -*- coding: utf-8 -*-
import csv
import re
import os
import logging
from collections import OrderedDict

logger = logging.getLogger('nipt_report_interpreter.nipt_report_parser')

EXTENDED_RAW_DATA = True

# report twin as "inkonklusiv" + no auto answer, if no autosome aneuploidy
TWIN_AS_INCONCLUSIVE = True

JOINCHAR = ";"

# check against anomaly_description
INTERPRET_SYSTEM_ERROR = {
    "CANCELLED": "Prøven ble annullert av brukeren",
    "INVALIDATED": "Prøven besto ikke kvalitetskontroll",
}

# check against anomaly_description
INTERPRET_ANORMALY = [
    ("+13", "Trisomi 13 (Pataus syndrom): Økt sannsynlighet"),
    ("+18", "Trisomi 18 (Edwards syndrom): Økt sannsynlighet"),
    ("+21", "Trisomi 21 (Downs syndrom): Økt sannsynlighet"),
    ("XO", "Kjønnskromosomavvik XO (Turner syndrom): Økt sannsynlighet"),
    ("XXX", "Kjønnskromosomavvik XXX (Trippel-X-syndrom): Økt sannsynlighet"),
    ("XXY", "Kjønnskromosomavvik XXY (Klinefelters syndrom): Økt sannsynlighet"),
    ("XYY", "Kjønnskromosomavvik XYY (Jacobs syndrom): Økt sannsynlighet"),
]

# check against class_sx
# NOT TESTED is handled lastly in INTERPRET_X_NORMALS
if TWIN_AS_INCONCLUSIVE:
    INTERPRET_CLASS_X_SPECIAL = [
        (["NOT REPORTABLE"], "Kjønnskromosomavvik: Programvaren kunne ikke rapportere kjønnskromosom"),
        (["NO CHR Y PRESENT", "CHR Y PRESENT"], "Kjønnskromosomavvik: Ikke mulig grunnet tvillinggraviditet, se metodebegrensninger.")
    ]
else:
    INTERPRET_CLASS_X_SPECIAL = [
        (["NOT REPORTABLE"], "Kjønnskromosomavvik: Programvaren kunne ikke rapportere kjønnskromosom")
    ]

# check against class_sx
GENDER_FROM_CLASS_X = {
    "NO ANOMALY DETECTED": "ikke rapportert",  # Negative sample and sex not reported
    "NO ANOMALY DETECTED - XX": "jente (XX)",
    "NO ANOMALY DETECTED - XY": "gutt (XY)",
    "NO CHR Y PRESENT": "Tvillinggraviditet (ikke påvist Y-kromosom)",
    "CHR Y PRESENT": "Tvillinggraviditet (påvist Y-kromosom)",
}

# check against anomaly_description
SYSTEM_ERROR = ["CANCELLED", "INVALIDATED"]
INTERPRET_NORMALS = [
    (["+13"] + SYSTEM_ERROR, "Trisomi 13: Lav sannsynlighet"),
    (["+18"] + SYSTEM_ERROR, "Trisomi 18: Lav sannsynlighet"),
    (["+21"] + SYSTEM_ERROR, "Trisomi 21: Lav sannsynlighet")
]

# check against class_sx
# ANOMALY DETECTED (DETECTED: XO|XXX|XXY|XYY) reported in INTERPRET_ANORMALY
# NOT REPORTABLE|NO CHR Y PRESENT|CHR Y PRESENT] reported in INTERPRET_CLASS_X_SPECIAL
# if anomaly_description(thus class_x): CANCELLED|INVALIDATED, do not report sx at all
# remaining: NO ANOMALY DETECTED( - X[XY])?[|NO CHR Y PRESENT|CHR Y PRESENT] as normal
if TWIN_AS_INCONCLUSIVE:
    INTERPRET_X_NORMALS = [
        (["NO ANOMALY DETECTED"], "Kjønnskromosomavvik: Lav sannsynlighet"),
        (["NOT TESTED"], "Kjønnskromosomavvik: Ikke undersøkt")
    ]
else:
    INTERPRET_X_NORMALS = [
        (["NO ANOMALY DETECTED", "NO CHR Y PRESENT", "CHR Y PRESENT"], "Kjønnskromosomavvik: Lav sannsynlighet"),
        (["NOT TESTED"], "Kjønnskromosomavvik: Ikke undersøkt")
    ]


# main result
# check against anomaly_description
# class_sx:NOT REPORTABLE; anomaly_description: NO ANOMALY DETECTED combination is treated the same as anomaly_description:INVALIDATED in get_main_result_swl()
MAIN_RESULT = {
    "NO ANOMALY DETECTED$": "Lav sannsynlighet for kromosomavvik",  # exact match
    "CANCELLED$": "Inkonklusiv",  # exact match
    "INVALIDATED$": "Inkonklusiv",  # exact match
    "DETECTED:": "Økt sannsynlighet for kromosomavvik",  # startswith
    "TWIN_NORMAL_AUTO": "Lav sannsynlighet for kromosomavvik (tvillinger)",  # value only
}


# method limitation depend on main result(no findings, with findings) and sex_chrom("yes", "no")
# note: following several changes, ML_NORMAL_NO and ML_ANORM_NO become the same; ML_NORMAL_YES and
# ML_ANORM_YES become the same; keep them separate for now in case further changes needed
ML_NORMAL_NO = "Den utførte NIPT-analysen er en screeningundersøkelse for trisomi 13, 18 og 21 hos fosteret. Hvis ultralydundersøkelsen viser avvik bør invasiv fosterdiagnostikk vurderes. Analysen kan ikke helt utelukke de nevnte kromosomavvik, da mosaikk i placenta og hos foster kan forekomme. Risiko for andre kromosomavvik vil ikke påvises.\n\nTestens sensitivitet og spesifisitet er over 99% for hver av trisomiene 13, 18 og 21. Ved tvillinggraviditeter er sensitivitet og spesifisitet noe redusert.\nEn rekke faktorer kan påvirke testens presisjon. Se litteraturen eller leverandørens informasjon for detaljer:\nhttps://emea.support.illumina.com/downloads/veriseq-nipt-solution-v2-package-insert-1000000078751.html"
ML_NORMAL_YES = "Den utførte NIPT-analysen er en screeningundersøkelse for trisomi 13, 18 og 21 samt kjønnskromosomavvik hos fosteret. Hvis ultralydundersøkelsen viser avvik bør invasiv fosterdiagnostikk vurderes. Analysen kan ikke helt utelukke de nevnte kromosomavvik, da mosaikk i placenta og hos foster kan forekomme. Risiko for andre kromosomavvik vil ikke påvises.\n\nTestens sensitivitet og spesifisitet er over 99% for hver av trisomiene 13, 18 og 21. Ved tvillinggraviditeter er sensitivitet og spesifisitet noe redusert. Testens sensitivitet er anslagsvis 80-90% for kjønnskromosomavvik ved singleton-graviditeter. Sannsynligheten for kjønnskromosomavvik kan ikke estimeres ved tvillinggraviditeter.\nEn rekke faktorer kan påvirke testens presisjon. Se litteraturen eller leverandørens informasjon for detaljer:\nhttps://emea.support.illumina.com/downloads/veriseq-nipt-solution-v2-package-insert-1000000078751.html"
ML_ANORM_NO = "Den utførte NIPT-analysen er en screeningundersøkelse for trisomi 13, 18 og 21 hos fosteret. Risiko for andre kromosomavvik vil ikke påvises.\n\nTestens sensitivitet og spesifisitet er over 99% for hver av trisomiene 13, 18 og 21. Ved tvillinggraviditeter er sensitivitet og spesifisitet noe redusert.\nEn rekke faktorer kan påvirke testens presisjon. Se litteraturen eller leverandørens informasjon for detaljer:\nhttps://emea.support.illumina.com/downloads/veriseq-nipt-solution-v2-package-insert-1000000078751.html"
ML_ANORM_YES = "Den utførte NIPT-analysen er en screeningundersøkelse for trisomi 13, 18 og 21 samt kjønnskromosomavvik hos fosteret. Risiko for andre kromosomavvik vil ikke påvises.\n\nTestens sensitivitet og spesifisitet er over 99% for hver av trisomiene 13, 18 og 21. Ved tvillinggraviditeter er sensitivitet og spesifisitet noe redusert. Testens sensitivitet er anslagsvis 80-90% for kjønnskromosomavvik ved singleton-graviditeter. Sannsynligheten for kjønnskromosomavvik kan ikke estimeres ved tvillinggraviditeter.\nEn rekke faktorer kan påvirke testens presisjon. Se litteraturen eller leverandørens informasjon for detaljer:\nhttps://emea.support.illumina.com/downloads/veriseq-nipt-solution-v2-package-insert-1000000078751.html"
METHOD_LIMITATION = {
    JOINCHAR.join([MAIN_RESULT["NO ANOMALY DETECTED$"], "no"]) : ML_NORMAL_NO,
    JOINCHAR.join([MAIN_RESULT["NO ANOMALY DETECTED$"], "yes"]) : ML_NORMAL_YES,
    JOINCHAR.join([MAIN_RESULT["DETECTED:"], "no"]) : ML_ANORM_NO,
    JOINCHAR.join([MAIN_RESULT["DETECTED:"], "yes"]) : ML_ANORM_YES,
    JOINCHAR.join([MAIN_RESULT["CANCELLED$"], "no"]) : ML_NORMAL_NO,
    JOINCHAR.join([MAIN_RESULT["CANCELLED$"], "yes"]) : ML_NORMAL_YES,
    JOINCHAR.join([MAIN_RESULT["INVALIDATED$"], "no"]) : ML_NORMAL_NO,
    JOINCHAR.join([MAIN_RESULT["INVALIDATED$"], "yes"]) : ML_NORMAL_YES,
    JOINCHAR.join([MAIN_RESULT["TWIN_NORMAL_AUTO"], "no"]) : ML_NORMAL_NO,
    JOINCHAR.join([MAIN_RESULT["TWIN_NORMAL_AUTO"], "yes"]) : ML_NORMAL_YES,
}

# follow up actions
# check against anomaly_description
# class_sx:NOT REPORTABLE; anomaly_description:NO ANOMALY DETECTED combination is treated the same as anomaly_description:INVALIDATED in compose_folow_up()
FOLLOW_UP = {
    "NO ANOMALY DETECTED$": "Vanligvis ikke aktuelt. Se metodebegrensninger.",  # exact match
    "CANCELLED$": "Vi tilbyr ny NIPT-analyse. Dersom dette er ønskelig, vennligst send ny rekvisisjon og blodprøve.",  # exact match
    "INVALIDATED$": "Vi tilbyr ny NIPT-analyse. Dersom dette er ønskelig, vennligst send ny rekvisisjon og blodprøve.",  # exact match; also for class_sx NOT REPORTABLE
    "DETECTED:": "Invasiv fosterdiagnostikk anbefales for å bekrefte/avkrefte resultatet, ettersom falskt positive resultater kan forekomme.\n\nVed ultralydfunn forenlig med trisomi kan morkakeprøve benyttes. Ved normal ultralyd anbefales morkakeprøve ved økt sannsynlighet for trisomi 21, og fostervannsprøve ved økt sannsynlighet for trisomi 13 eller 18.",  # startswith
}


def report_system_error(anomaly_description):
    """
    CANCELLED/INVALIDATED
    """

    system_error = ""

    anomaly_description = anomaly_description.strip()
    for k, v in INTERPRET_SYSTEM_ERROR.items():
        if anomaly_description == k:
            system_error = v
            break

    return system_error


def _get_findings_list(anomaly_description):
    """
    returns a list of findings
    """

    PREFIX = 'DETECTED:'

    # remove leading space
    findings = anomaly_description.strip()

    # remove prefix
    if findings.startswith(PREFIX):
        findings = findings[len(PREFIX):]

    # get list of semicolon separated findings
    findings = findings.split(';')

    # remove spaces
    findings = [f.strip() for f in findings]

    return findings


def report_findings(anomaly_description):
    """
    report findings
    """

    findings_interpret = []

    findings = _get_findings_list(anomaly_description)

    for k, v in INTERPRET_ANORMALY:
        if k in findings:
            findings_interpret.append(v)

    return '\n'.join(findings_interpret)


def repor_sx_system_special(class_sx):
    """
    class_sx is NOT REPORTABLE/Twin(not reportable by default)
    """

    sx_system_error = ""

    class_sx = class_sx.strip()

    for sx, v in INTERPRET_CLASS_X_SPECIAL:
        if any(class_sx == s for s in sx):
            sx_system_error = v
            break

    return sx_system_error


def report_normals_auto(anomaly_description):
    """
    state normal if no abnormal for class_auto
    """

    findings = _get_findings_list(anomaly_description)

    normals = []

    for anom_s, normal_interpretation in INTERPRET_NORMALS:
        if not any(anom in findings for anom in anom_s):
            normals.append(normal_interpretation)

    return '\n'.join(normals)


def report_normals_sx(class_sx):
    """
    report sx normal
    """

    sx_norm_interpretation = ""

    class_sx = class_sx.strip()

    for patn_s, sx_norm_interpret in INTERPRET_X_NORMALS:
        if any(re.match(p, class_sx) for p in patn_s):
            sx_norm_interpretation = sx_norm_interpret

    return sx_norm_interpretation


def report_gender(class_sx):
    """
    kjønn in comment's RESULT section
    """

    gender = None

    if class_sx.strip() in GENDER_FROM_CLASS_X:
        gender = GENDER_FROM_CLASS_X[class_sx]
        gender = "Kjønn: " + gender

    return gender


def report_ff(ff):
    """
    ff in comment's RESULT section
    """
    if str(ff).strip() in ["INVALIDATED", "CANCELLED"]:
        return "Føtalfraksjon: Ugyldig"
    else:
        return "Føtalfraksjon: " + str(ff)


def compose_result_in_comment(anomaly_description, class_sx, ff, sex_chrom):
    """
    make the [RESULT] part of comment_swl column
    """

    result_in_comment = ""

    # report system error, e.g. CANCELLED, INVALIDATED, if it is
    system_error = report_system_error(anomaly_description)
    if system_error:
        result_in_comment += system_error + "\n"

    # report findings if any
    findings_interpret = report_findings(anomaly_description)
    if findings_interpret:
        result_in_comment += findings_interpret + "\n"

    # report class_sx system error, e.g. NOT REPORTABLE, Twin(not reportable by default)
    class_sx_special = repor_sx_system_special(class_sx)
    if class_sx_special:
        result_in_comment += class_sx_special + "\n"

    # state auto normal if neither abnormal nor system error
    normals_auto_interpret = report_normals_auto(anomaly_description)
    if normals_auto_interpret:
        result_in_comment += normals_auto_interpret + "\n"

    # state sx normal when sex chroms tested and is normal
    if sex_chrom.lower() == 'yes':
        sx_normal = report_normals_sx(class_sx)
        if sx_normal:
            result_in_comment += sx_normal + "\n"

    # report gender
    gender = report_gender(class_sx)
    if gender:
        result_in_comment += gender + "\n"

    # report ff
    ff_comment = report_ff(ff)
    result_in_comment += ff_comment  # last entry of RESULT

    return result_in_comment


def compose_folow_up(anomaly_description, class_sx):
    """
    make the [OPPFØLGING/VIDERE UTREDNING] part of comment_swl column
    """

    follow_up_action = ""

    anomaly_description = anomaly_description.strip()
    class_sx = class_sx.strip()

    for anom_patn, act in FOLLOW_UP.items():
        if re.match(anom_patn, anomaly_description):
            follow_up_action = act
            break

    # special handling of class_sx: NOT REPORTABLE
    if class_sx == 'NOT REPORTABLE' and anomaly_description == 'NO ANOMALY DETECTED':
        follow_up_action = FOLLOW_UP['INVALIDATED$']

    if TWIN_AS_INCONCLUSIVE:
        # Twin follow-up when no autosome aneupoidy
        if class_sx in ["CHR Y PRESENT", "NO CHR Y PRESENT"] and anomaly_description == 'NO ANOMALY DETECTED':
            follow_up_action = FOLLOW_UP['NO ANOMALY DETECTED$']

    return follow_up_action


def compose_method_limitations(result, sex_chrom):
    """
    make [METODEBEGRENSNINGER] part of comment_swl column
    """

    sex_chrom = sex_chrom.strip().lower()

    key = JOINCHAR.join([result, sex_chrom])

    return METHOD_LIMITATION[key]


def compose_raw_data(sample):
    """make raw data part of comment_swl column

    @param sample: a sample in nipt report
    @type  sample: dict

    @return: raw data for RESULT part in comment_swl
    @rtype: string
    """

    DELIM = ', '
    RAW_DATA_LINE_LENTH = 130
    RAW_DATA_LINE_PREFIX = "# "

    raw = 'batch:' + ' ' + str(sample['batch_name']) + DELIM + \
        'sample_barcode:' + ' ' + str(sample['sample_barcode']) + DELIM + \
        'qc_flag:' + ' ' + str(sample['qc_flag']) + DELIM + \
        'qc_reason:' + ' ' + str(sample['qc_reason']) + DELIM + \
        'og fetal_fraction(ff):' + ' ' + str(sample['ff'])

    # append more raw data
    if EXTENDED_RAW_DATA:
        raw = raw + DELIM + \
            'sample_type:' + ' ' + str(sample['sample_type']) + DELIM + \
            'sex_chrom:' + ' ' + str(sample['sex_chrom']) + DELIM + \
            'screen_type:' + ' ' + str(sample['screen_type']) + DELIM + \
            'class_sx:' + ' ' + str(sample['class_sx']) + DELIM + \
            'class_auto:' + ' ' + str(sample['class_auto']) + DELIM + \
            'anomaly_description:' + ' ' + str(sample['anomaly_description'])

    # format raw data lines
    elements = raw.split(DELIM)
    formatted_raw = RAW_DATA_LINE_PREFIX + elements.pop(0)
    line_length = len(formatted_raw)
    for e in elements:
        line_length = line_length + len(DELIM) + len(e)
        if line_length <= RAW_DATA_LINE_LENTH:
            formatted_raw = formatted_raw + DELIM + e
        else:
            _adding = RAW_DATA_LINE_PREFIX + DELIM + e  # add newline prefix
            formatted_raw = formatted_raw + "\n" + _adding  # add line break
            line_length = len(_adding)  # update newline length

    return formatted_raw


def get_main_result_swl(sample):
    """genereate results_swl for a sample

    @param sample: a sample in nipt report
    @type  sample: dict

    @return: interpreted results for results_swl
    @rtype: string

    """

    main_result = ""

    class_sx = sample['class_sx'].strip()
    anomaly_description = sample['anomaly_description'].strip()

    for anom_patt, summary in MAIN_RESULT.items():
        if re.match(anom_patt, anomaly_description):
            main_result = summary
            break

    # special handling of class_sx NOT REPORTABLE
    if class_sx == "NOT REPORTABLE" and anomaly_description == 'NO ANOMALY DETECTED':
        main_result = MAIN_RESULT["INVALIDATED$"]

    if TWIN_AS_INCONCLUSIVE:
        # special handling of Twin when autosome eupoidy
        if class_sx in ["CHR Y PRESENT", "NO CHR Y PRESENT"] and anomaly_description == 'NO ANOMALY DETECTED':
            main_result = MAIN_RESULT["TWIN_NORMAL_AUTO"]

    return main_result


def get_comment_swl(main_result, sample):
    """genereate comment_swl for a sample

    @param sample: a sample in nipt report
    @type  sample: dict

    @return: interpreted comment for comment_swl
    @rtype: string
    """

    result = compose_result_in_comment(
        sample['anomaly_description'],
        sample['class_sx'],
        sample['ff'],
        sample['sex_chrom'])

    followup = compose_folow_up(sample['anomaly_description'],
                                sample['class_sx'])

    method_limitation = compose_method_limitations(main_result, sample['sex_chrom'])

    raw_data = compose_raw_data(sample)

    comment_swl = "RESULTAT:\n" + result + "\n\n" + \
        "OPPFØLGING/VIDERE UTREDNING:\n" + followup + "\n\n" + \
        "METODEBEGRENSNINGER:\n" + method_limitation + "\n\n" + \
        raw_data

    return comment_swl


def get_auto_answering(sample):
    """genereate automatic_answering_swl for a sample

    @param sample: a sample in nipt report
    @type  sample: dict

    @return: yes or no for automatic_answering_swl
    @rtype: string
    """
    _yes = "Ja"
    _no = "Nei (lege må svare ut)"

    if sample['anomaly_description'].strip() == 'NO ANOMALY DETECTED':
        # special handling of class_sx NOT REPORTABLE
        if TWIN_AS_INCONCLUSIVE:
            NO_AUTO = ['NOT REPORTABLE', 'CHR Y PRESENT', 'NO CHR Y PRESENT']
        else:
            NO_AUTO = ['NOT REPORTABLE']

        if sample['class_sx'].strip() in NO_AUTO:
            auto_ans = _no
        else:
            auto_ans = _yes
    else:
        auto_ans = _no

    return auto_ans


def get_analyte_code(sample):
    """
    @param sample: a sample in nipt report
    @type  sample: dict

    @return: based on sex_chrom value
    @rtype: string
    """

    analyte_code = ''

    sex_chrom = sample['sex_chrom'].strip().lower()

    if sex_chrom == 'yes':
        analyte_code = 'Basic_Yes'
    elif sex_chrom == 'no':
        analyte_code = 'Basic_No'
    else:
        raise RuntimeError("unknown option '{}' for sex_chrom".format(sex_chrom))

    return analyte_code


def parse_report(nipt_report, output_dir=None):
    """
    parse a nipt report
    generate 2 output files:
        * .csv file as backup solution("\n" in comment is newline)
        * .tsv file for SWL import("\n" in comment is literal)

    @param nipt_report: a nipt report
    @type  nipt_report: string(path)

    @param output_dir: path to a dir to write interpreted report
    @type  output_dir: string(path)
    """

    report_name_core = os.path.splitext(os.path.basename(nipt_report))[0]
    outfile_name_excel = report_name_core + '.csv'
    outfile_name_swl = 'swl_' + report_name_core + '.tsv'

    if output_dir:
        output_report_excel = os.path.join(output_dir, outfile_name_excel)
        output_report_swl = os.path.join(output_dir, outfile_name_swl)
    else:
        # cwd
        output_report_excel = outfile_name_excel
        output_report_swl = outfile_name_swl

    with (
        open(nipt_report)) as report, (
        open(output_report_excel, 'w', encoding='utf-16', newline='')) as outfile_excel, (
        open(output_report_swl, 'w', encoding='utf-16', newline='')) as outfile_swl:

        # reader ignores comment lines in the original nipt report
        reader = csv.DictReader(filter(lambda row: row.lstrip()[0] != '#', report), delimiter='\t')

        writer_excel = csv.writer(outfile_excel, delimiter='\t', lineterminator='\n')
        writer_swl = csv.writer(outfile_swl, delimiter='\t', lineterminator='\n')

        # # excel trick to force comma as separator
        # outfile.write("SEP=,\n")

        # write header
        header = [
            'sample',
            'results_swl',
            'comment_swl',
            'automatic_answering_swl',
            'analyte_code'
        ]

        writer_excel.writerow(header)
        writer_swl.writerow(header)

        # write data
        for row in reader:
            # skip NTC
            if '_NTC_' in row['sample_barcode']:
                continue
            sample = row['sample_barcode']
            # duplicated samples for making up a new batch should not be answered out(should not be
            # imported into SWL)
            # these duplicated samples' barcodes always end with another number than "01"
            if sample[-2:] != "01":
                logger.info("skipped duplicated sample %s", sample)
                continue
            result = get_main_result_swl(row)
            comment_excel = get_comment_swl(result, row)  # comment for excel
            comment_swl = get_comment_swl(result, row).replace('\n', '\\n')  # comment for SWL, literal "\n"
            auto_answer = get_auto_answering(row)
            analyte_code = get_analyte_code(row)

            writer_excel.writerow([sample, result, comment_excel, auto_answer, analyte_code])
            writer_swl.writerow([sample, result, comment_swl, auto_answer, analyte_code])


if __name__ == '__main__':
    import sys
    parse_report(sys.argv[1])
