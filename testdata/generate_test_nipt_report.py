#!/usr/bin/env python

from pprint import pprint
from numpy.random import randint

CLASS_SX = [
    "ANOMALY DETECTED",
    "NO ANOMALY DETECTED",
    "NO ANOMALY DETECTED - XX",
    "NO ANOMALY DETECTED - XY",
    "NOT REPORTABLE",
    "NO CHR Y PRESENT",
    "CHR Y PRESENT",
    "CANCELLED",
    "INVALIDATED",
    "NOT TESTED",
]

CLASS_SX_NO_FINDING = [  # not any of XO, XXX, XXY, XYY
    "NO ANOMALY DETECTED",
    "NO ANOMALY DETECTED - XX",
    "NO ANOMALY DETECTED - XY",
    "NOT REPORTABLE",
    "NO CHR Y PRESENT",
    "CHR Y PRESENT",
    "CANCELLED",
    "INVALIDATED",
    "NOT TESTED",
]

ANOMALY_DESCRIPTION = [
    "NO ANOMALY DETECTED",
    "INVALIDATED",
    "CANCELLED",
    "DETECTED: +13",
    "DETECTED: +18",
    "DETECTED: +21",
    "DETECTED: XO",
    "DETECTED: XXX",
    "DETECTED: XXY",
    "DETECTED: XYY",
    "DETECTED: +13; +18",
    "DETECTED: +13;+21",
    "DETECTED: +13; XO",
    "DETECTED: +13;XXX",
    "DETECTED: +13; XXY",
    "DETECTED: +13; XYY",
    "DETECTED: +18;+21",
    "DETECTED: +18; XO",
    "DETECTED: +18;XXX",
    "DETECTED: +18; XXY",
    "DETECTED: +18; XYY",
    "DETECTED: +21; XO",
    "DETECTED: +21;XXX",
    "DETECTED: +21; XXY",
    "DETECTED: +21; XYY",
    "DETECTED: +13; +18; +21",
    "DETECTED: +13; +18; XXX",
]

headers = ["batch_name", "sample_barcode", "sample_type", "sex_chrom", "screen_type", "flowcell", "class_sx", "class_auto", "anomaly_description", "qc_flag", "qc_reason", "ff"]
batch_name = "Batch210222"
sample_barcode_stem = "20210222"
sample_type = "Singleton"
sex_chrom = "yes"
screen_type = "basic"
flowcell = "H5TT5BDXZ"
qc_flag = "PASS"
qc_reason = "NONE"


def check_combination(class_sx, anomaly_description):
    reasonable = True

    sx_anom = ["XO", "XXX", "XXY", "XYY"]
    system_error = ["INVALIDATED", "CANCELLED"]

    if class_sx == "ANOMALY DETECTED" and not any(a in anomaly_description for a in sx_anom):
        reasonable = False
    if class_sx == "ANOMALY DETECTED" and anomaly_description == "NO ANOMALY DETECTED":
        reasonable = False

    if class_sx in CLASS_SX_NO_FINDING and any(a in anomaly_description for a in sx_anom):
        reasonable = False

    if anomaly_description in system_error and anomaly_description != class_sx:
        reasonable = False

    if class_sx in system_error and anomaly_description != class_sx:
        reasonable = False

    return reasonable


def get_class_auto(anomaly_description):

    class_auto = None

    if any(a in anomaly_description for a in ["+13", "+18", "+21"]):
        class_auto = "ANOMALY DETECTED"
    elif anomaly_description in ["INVALIDATED", "CANCELLED"]:
        class_auto = anomaly_description
    else:
        class_auto = "NO ANOMALY DETECTED"

    return class_auto


def get_qc_flag(anomaly_description):
    if anomaly_description == "INVALIDATED":
        return "FAIL"
    else:
        return "PASS"


def get_qc_reason(anomaly_description):
    if anomaly_description == "INVALIDATED":
        return "FAILED iFACT"
    else:
        return "NONE"


def get_ff(anomaly_description):

    ff = ""

    if anomaly_description in ['INVALIDATED', 'CANCELLED']:
        ff = anomaly_description
    else:
        ratio = randint(21)
        if ratio < 1:
            ratio = '< 1'
        ff = "{}%".format(randint(21))

    return ff


sx_anomaly_combinatoins = []

for sx in CLASS_SX:
    for anom in ANOMALY_DESCRIPTION:
        sx_anomaly_combinatoins.append((sx, anom))

i = 0
print("\t".join(headers))
for sx, anom in sx_anomaly_combinatoins:
    reasonable = check_combination(sx, anom)
    if not reasonable:
        continue
    i += 1
    if sx in ["CHR Y PRESENT", "NO CHR Y PRESENT"]:
        correct_sample_type = "Twin"
    else:
        correct_sample_type = sample_type
    print(batch_name, end="\t")
    print(sample_barcode_stem + "{:03d}".format(i), end="\t")
    print(correct_sample_type, end="\t")
    print(sex_chrom, end="\t")
    print(screen_type, end="\t")
    print(flowcell, end="\t")
    print(sx, end="\t")
    print(get_class_auto(anom), end="\t")
    print(anom, end="\t")
    print(get_qc_flag(anom), end="\t")
    print(get_qc_reason(anom), end="\t")
    print(get_ff(anom), end="\n")
