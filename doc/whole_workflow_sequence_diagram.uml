@startuml
!include https://raw.githubusercontent.com/bschwarz/puml-themes/master/themes/bluegray/puml-theme-bluegray.puml
!define FONTAWESOME https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/font-awesome
!define FONTAWESOME5 https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/font-awesome-5
!include FONTAWESOME/file_text_o.puml
!include FONTAWESOME/folder.puml
!include FONTAWESOME5/long_arrow_alt_right.puml
!include FONTAWESOME5/long_arrow_alt_left.puml
!include FONTAWESOME5/list_ol.puml
!include FONTAWESOME5/file_excel.puml

' !pragma teoz true
' sprite
sprite $clarity_logo [107x32/16z] {
xTPLmkem68JXcKzd_nj6qhG7kIbwx7-nMlBWWDsH-4fUaZe8MFi5tysnq4lbum3hjUp2ADkmgrIvUQe8qmKLKeu5LPpAbiCKF2-FJHuAdFBbHkSgCwlSxTIT
Fmz4AAueTgQ8KrCq9Mmvc7DCoG2gi8_gvfowhoYNcUA2uZt5RhAQEoabjYwfBc2PHSbp8RkbuCgKOHSrHzrJBfzNqCnmHo4r5nzwLBHBgfIdr01KVYhLlAVG
ZTrKzLpekg1CFP83okrntbDL-wcOmrnnGSqt7rKRHSMB54hfvgK2JEaZAbVIi2uhNwKGBQa3Pses7zJ0DwhhPGgW4I-qctgy7-g78lakAffpgHEepfNeTi1R
KlkRA5rIhNKr98UhtBmKdw1wFYogvEjtSVKdg9hh8PUlFz6qZr1K0c27oH7gT2Fm4GefZgqcdt_SLFOPYWgmS_tnedbA_0Y5R8PuffWsZ_7FK5HqOb2gzO_Q
UwZmqKOX9HiKKkLcqSttK4lrYp8jrpv_eLNypdUW6V4dTGG
}
' define variable
!$step3="<b><color:green>AUTOMATED - Data Analysis</color>"
!$step1="<b><color:green>Generate Batch & Input Sample Sheet</color>"
!$lifeline_color = "Teal"
!$text_file_icon_2 = "color:DarkTurquoise><$file_text_o></color"
!$text_file_icon_3 = "color:DarkSalmon><$file_text_o></color"
!$text_file_icon_4 = "color:DeepPink><$file_text_o></color"
!$text_file_icon_5 = "color:MediumBlue><$file_text_o></color"
!$text_file_icon_6 = "color:LawnGreen><$file_text_o></color"
!$text_file_icon_7 = "color:BlueViolet><$file_text_o></color"
' skinparam
'skinparam responseMessageBelowArrow true
skinparam shadowing false
skinparam defaultFontSize 15
skinparam sequenceBoxFontSize 20
skinparam Sequence {
    LifeLineBackgroundColor Ivory
    LifeLineBorderThickness 3
    LifeLineBorderColor $lifeline_color
    DividerBorderColor Black
    ActorBorderColor Gold
    DelayFontName Chalkboard
}
skinparam actorStyle awesome
skinparam arrowThickness 2
skinparam arrowColor DarkRed
skinparam Sequence {
    GroupBackgroundColor Gray
    GroupBorderColor Black
    GroupFontColor OrangeRed
    GroupHeaderFontColor Black
    GroupBodyBackgroundColor #E5E7E990
    GroupHeaderBackgroundColor Gray
}
skinparam participant {
    BackgroundColor<< LIMS >> DarkSeaGreen
}
skinparam NoteBackgroundColor yellow
skinparam NoteFontColor DimGray

autonumber "<b>[#]"
skinparam sequenceMessageAlignment left
skinparam SequenceBoxBorderColor transparent
' hide footbox

title NIPT Workfow

center header NIPT Workflow focusing on software

' declare actors/participants
actor Lab #green
participant "Sykehuspartner\nExcel Macro" as Sykehuspartner <<(S,green) software >>
participant "SwissLab\n" as SwissLab <<(A,Gold) LIMS >>
participant "FellesArbeidsArk\nExcel Macro" as FellesArbeidsArk <<(S,green) software >>
participant "Hamilton\nSTAR" as hamilton <<(I,orchid) instrument>>
' grouping
box AUTOMATION #DarkSalmon
participant "VeriSeq\nServer" as VS <<(V,red) server>>
participant "<$clarity_logo,color=#2B8DD4>" as Clarity <<(A,Gold) LIMS >>
participant "Custom\nClarity Integration" as CA <<(S,green) software >> 
participant "Clarity\nIntegration" as CI <<(S,green) software >>
participant "Automatic\nSWL Import" as ASI <<(S,green) software >>
end box

' daemon
activate CA #$lifeline_color
activate CI #$lifeline_color
activate ASI #$lifeline_color

group Register NIPT samples in Clarity
    activate Lab #$lifeline_color
    Lab ->  Sykehuspartner ++ #$lifeline_color : retrieve pending NIPT samples in SwissLab(SWL)
    Sykehuspartner -> SwissLab ++#$lifeline_color  : return pending NIPT samples
    SwissLab --> Sykehuspartner -- : list <$list_ol> of pending NIPT samples
    Sykehuspartner --> Lab -- : an excel file <$file_excel> of pending NIPT samples
    Lab -> FellesArbeidsArk ++#$lifeline_color  : generate **LIMS sheet** for pending NIPT samples
    FellesArbeidsArk --> Lab -- : **LIMS sheet** <color:DarkTurquoise><$file_excel></color> ready for Clarity upload
    |||
    == Clarity LIMS ==
    |||

    Lab -> "Clarity\nLIMS" as Clarity : create a new NIPT project in Clarity
    activate Clarity #$lifeline_color
    Lab -> Clarity : upload the **LIMS sheet** <color:DarkTurquoise><$file_excel></color> to Clarity
end
Lab -> Clarity : assign samples to the <b><color:green>Veriseq NIPT v1.0_EHD</color></b> workflow
|||
== Hamilton STAR & VeriSeq ==
|||
note over Lab, hamilton
    Can load rack of samples to Hamilton before importing samples into Clarity but this is <color:red>not recommneded</color>
end note
Lab -> "Hamilton\nSTAR" as hamilton : load a rack of samples
hamilton -> "VeriSeq\nServer" as VS ++#$lifeline_color  : <$file_text_o> batch initiation report\nwith sample barcodes
deactivate Lab

... <font size=30 color=gray>lab prep, sequencing and VeriSeq analysis skipped, here and after</font> ...

loop every 1 hour (cron job) 
    CA -> Clarity : remove duplicated samples in <b><color:green>Generate Batch & Input Sample Sheet</color></b> queue
end

loop every 3 minutes (cron job)
    alt new <i>ad hoc</i> batch initiation report generated
        VS -[#blue]> CA : <$file_text_o>new //ad hoc// batch initiation report
        CA --> Clarity : check if all samples in the //ad hoc// batch initiation report are in\n<b><color:green>Generate Batch & Input Sample Sheet</color> queue
        alt Yes 
            Clarity -> CA : all samples in queue, ready to start a Step
            CA --/ Clarity : remove container name suffix "_NIPT" if any
            CA --/ Clarity : start a <b><color:green>Generate Batch & Input Sample Sheet</color> **step**\nwith all samples
            CA --/ Clarity : update each artifact UDF "NIPT Pool" ("A" or "B")
            CA --/ Clarity : update project UDF "Project type" ("NIPT")
            activate Clarity #Plum
            CA --/ Clarity : register Batch ID in //ad hoc// batch initiation report to the **step**
            CA --> Clarity : continue the **step**
            Clarity -> CI : generate **input sample sheet** and copy over to\na network folder<$folder>
            activate CI #Plum
            CI -> CI : generate **input sample sheet**<color:DarkTurquoise><$file_text_o></color> 
            CI -> CI : copy **input sample sheet** <color:DarkTurquoise><$file_text_o></color> to\na **NIPT-lab**<$folder> folder
            CI -> Clarity : done
            deactivate CI
            deactivate Clarity
        end
    end
end
note left : <i>Start an //ad hoc// batch in Clarity</i>

Clarity o-[#SlateBlue]> Lab ++ #$lifeline_color : register sequencing reagent lots and instruments
loop if 96-sample batch; do this twice, once for //pool A//, once for //pool B//
    Lab -> Clarity : register sequencing reagent lots and instruments
    Lab -> Clarity -- : complete <b><color:green>"Register NIPT Instrument"</color> step
end

||100||

loop every 30 minutes
    alt new batch initiation report<color:DimGrey><$file_text_o></color>
        VS -[#blue]> CA : <$file_text_o>new batch initiation report
        activate CA #Plum
        CA --> Clarity : check if all samples in the batch initiation report are in\nthe $step3 queue?
        activate Clarity #Plum
        alt Yes
            Clarity-> CA : yes, all samples are in $step3 queue
            CA -> CI : <$file_text_o>process new batch initiation report
            activate CI #Plum
            CI -> Clarity : start $step3 step
            deactivate CI
        else No
            Clarity -> CA : no, some samples are not found\nin $step3 queue
            CA -> CA : wait 30 minutes and try again
        end
        deactivate Clarity
        deactivate CA
    end
end
note left : <i>forward batch initiation report</i><$file_text_o>\n<i>to Clarity NIPT Integration</i>\n<i>by Custom Clarity Integration</i>

||100||

loop every 5 minutes
    alt new library reagent report<$text_file_icon_2>
        VS -[#blue]> CA : <$text_file_icon_2>new library reagent report
        CA -> Clarity : check if there is a $step1\nstep whose Batch ID matches the ID in the report
        alt yes
            Clarity -> CA : found the step
            CA -> CA : register lab prep reagent lots to the step UDFs
            CA --> Clarity : register lab prep reagent lots to the step UDFs
        end
    end
end
note left : <i>Automatically register (pre-sequencing)reagent lots in Clarity</i>
||100||
loop every 30 minutes
    alt new sample invalidation report<$text_file_icon_3>, cancellation report<$text_file_icon_4> or NIPT report<$text_file_icon_5> generated
        par parallel processing
            VS -[#blue]> CA : <$text_file_icon_3>new sample invalidation report
            activate CA #Plum
        else
            VS -[#blue]> CA : <$text_file_icon_4>new sample cancellation report found
            activate CA #Coral
        else
            VS -[#blue]> CA : <$text_file_icon_5>new NIPT report
            activate CA #Khaki

        end


        par Check in Clarity
            CA --> Clarity : check if the invalidated sample is in\na started <$step3> step
        else
            CA --> Clarity : check if the cancelled sample is in\na started <$step3> step
        else
            CA --> Clarity : check if all the NIPT report samples are in\na started <$step3> step

        end


        par
            alt all samples in NIPT report are in a started $step3 step
                CA -> Clarity : rename all duplicated containers by appending "_NIPT" to container name
                CA -> CI : <$text_file_icon_5> found a new NIPT report
                deactivate CA
                activate CI #Khaki
            end
        else
            alt the cancelled sample is in a started $step3 step
                CA -> CI : <$text_file_icon_4> found a new sample cancellation report found
                deactivate CA
                activate CI #Coral
            end
        else
            alt the invalidation sample is in a started $step3 step
                CA -> CI : <$text_file_icon_3> found a new sample invalidation report
                deactivate CA
                activate CI #Plum
            end
        end


        CI -> CI : process received report(s)
        CI -> CI : update Clarity
        CI --> Clarity : check if all samples in the started $step3 step have received their report(s)
        alt No
            Clarity -> CI : still samples missing report(s)
            CI -> CI : wait until next check\n(max 5 checks, if reached, ignore report permanently)
        else Yes
            Clarity -> CI : all samples received their reports
            CI --> Clarity : complete $step3 step
            deactivate CI
            deactivate CI
            deactivate CI
            deactivate Clarity
        end
    end
end
note left : <i>Process:\n*<i>sample invalidation report<$file_text_o>\n*<i>sample cancellation report<$file_text_o>\n*<i>NIPT report<$file_text_o>

||100||
loop every 30 minutes
    alt new NIPT report<$text_file_icon_5>
        VS -> CA ++ #Plum : <$text_file_icon_5>new NIPT report
        CA -> CA : generate interpreted NIPT report for\nautomatic SWL import (.tsv) <$text_file_icon_6>\n(ignore samples whose id does not end with "01")
        CA -> CA : generate interpreted NIPT report for\nmanual SWL import(.csv, backup solution) <$text_file_icon_7>\n(ignore samples whose id does not end with "01")
        CA -> CA -- : copy both reports <$text_file_icon_6> <$text_file_icon_7> to the NIPT-lab folder<$folder>
    end
end
note left
    <i>tranform/interpret original NIPT report<$file_text_o> for importing to SWL;</i>
    <i> generate a separate file with the same content for backup solution.</i>
end note

CA -[#blue]> Lab ++ #$lifeline_color : ready to transfer interpreted NIPT report<$text_file_icon_6> to OUS network

Lab -> Lab : <$text_file_icon_6>copy interpreted NIPT report to OUS network with ironkey

Lab -[#green]> ASI -- #$lifeline_color : <$text_file_icon_6> new interpreted NIPT report to import to SWL

ASI -> ASI : import interpreted NIPT report <$text_file_icon_6> into SWL

legend left
    |= style |=         meaning|
    | <color:green><$long_arrow_alt_right></color> | automatically trigerred |
    | <color:blue><$long_arrow_alt_right></color> | receiver actively check if sender has the message |
endlegend

center footer [version 2.2.0 - last updated %date()]

@enduml
