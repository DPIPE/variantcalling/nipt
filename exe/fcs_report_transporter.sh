#!/bin/bash

set -euf -o pipefail

DIR="$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)"

export PATH="${DIR}/../../anaconda/bin/":${PATH}
export PYTHONPATH="${DIR}/../src:${DIR}/../../site-packages":${PYTHONPATH=}

CMD="python ${DIR}/../src/fcs_reports/transport_fcs_reports.py $*"

# CMD="python ${DIR}/../src/fcs_reports/transport_fcs_reports.py --veriseq-output-dir ${DIR}/../testdata/veriseq_output --fcs-monitor-dir ${DIR}/../testdata/fcs_report --processed-reports-file ${DIR}/../testdata/processed_fcs_reports.txt"

# CMD="python ${DIR}/../src/fcs_reports/transport_fcs_reports.py --veriseq-output-dir ../testdata/veriseq_output --fcs-monitor-dir ../testdata/fcs_report --processed-reports-file ../testdata/processed_fcs_reports.txt"

$CMD
