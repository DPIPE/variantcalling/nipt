#!/bin/bash

set -euf -o pipefail

DIR="$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)"

export PYTHONPATH="${DIR}/../src":${PYTHONPATH=}

CMD="python ${DIR}/../src/adhoc_batching/monitor_adhoc_batch.py $*"

# CMD="python ${DIR}/../src/adhoc_batching/monitor_adhoc_batch.py --veriseq-output-dir ${DIR}/../testdata/veriseq_output --report-match .*_batch_initiation_report_.*.tab$ --processed-reports-file ${DIR}/../testdata/processed_adhoc_batch_initionation_reports.txt --queue-id 1302"

# CMD="python ${DIR}/../src/adhoc_batching/monitor_adhoc_batch.py --veriseq-output-dir ../testdata/veriseq_output --report-match .*_batch_initiation_report_.*.tab$ --processed-reports-file ../testdata/processed_adhoc_batch_initionation_reports.txt --queue-id 1302"

$CMD
