#!/bin/bash

set -euf -o pipefail

DIR="$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)"

export PYTHONPATH="${DIR}/../src":${PYTHONPATH=}

CMD="python ${DIR}/../src/lot_instrument_tracing/track_lot_intrument.py $*"

# CMD="python ${DIR}/../src/lot_instrument_tracing/track_lot_intrument.py --veriseq-output-dir ${DIR}/../testdata/veriseq_output --processed-reports-file ${DIR}/../testdata/processed_reagent_labware_reports.txt"

# CMD="python ${DIR}/../src/lot_instrument_tracing/track_lot_intrument.py --veriseq-output-dir ../testdata/veriseq_output --processed-reports-file ../testdata/processed_reagent_labware_reports.txt"

$CMD
