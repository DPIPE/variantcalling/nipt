#!/bin/bash

set -euf -o pipefail

DIR="$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)"

export PATH="${DIR}/../../anaconda/bin/":${PATH}
export PYTHONPATH="${DIR}/../src:${DIR}/../../site-packages":${PYTHONPATH=}

CMD="python ${DIR}/../src/duplicate_handling/duplicates_deleter/delete_duplicates.py $*"

# CMD="python ${DIR}/../src/duplicate_handling/duplicates_deleter/delete_duplicates.py --queue-id 1302 --workflow-id 2001"  # prod

# CMD="python ${DIR}/../src/duplicate_handling/duplicates_deleter/delete_duplicates.py --queue-id 3101 --workflow-id 2551"  # dev

$CMD
