#!/usr/bin/env python
import sys
sys.path.insert(0, '../src')
import requests
from genologics.lims import *
from genologics.entities import Process, Queue, Step
from genologics.config import USERNAME, PASSWORD, BASEURI
from urllib3.exceptions import InsecureRequestWarning
from xml.etree import ElementTree
import time

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

#ssl.match_hostname = lambda cert, hostname: True

lims = Lims(BASEURI, USERNAME, PASSWORD)

queue = Queue(lims, id='1123')

step_art = queue.artifacts + ['https://localhost:9443/api/v2/controltypes/302']

step = Step.create(lims, queue.protocol_step_uri, step_art)

proc=Process(lims,id=step.id)

proc.udf['Batch ID'] = 'Batch210311F'
proc.put()
#
#time.sleep(60)  # must wait a bit, otherwise trigger external program fails
#
#for prog in step.available_programs:
#    if prog.name == 'Check Batch & Generate Sample Sheet':
#        prog.trigger()
#        break
#
from IPython import embed
embed()
