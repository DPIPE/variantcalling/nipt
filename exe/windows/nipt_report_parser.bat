<# : nipt_report_parser.bat
:: launches a file chooser to choose original nipt report(s) process lines and output a new csv


@echo off
setlocal EnableDelayedExpansion
for /f "delims=" %%I in ('powershell -noprofile "iex (${%~f0} | out-string)"') do (
	set "output_file=%%~dpnI.csv"
	echo You chose %%~I
	if exist "!output_file!" (
		set /p overwrite="!output_file! already exists, overwrite[yes,no]? "
		set "_choice="
		if /I !overwrite! EQU yes set "_choice=yes"
		if /I !overwrite! EQU y set "_choice=yes" 
		if !_choice! EQU yes (
			del "!output_file!" && echo !output_file! has been deleted 
		) else (
			echo appending, output file may have duplicated rows!
		)
	)
	echo Generating final file...
	call :parse "%%~I"
)


:parse
(
for /f "usebackq tokens=1-12 delims=	" %%G in (%1) do (
    set "_batch_name=%%G"
	set "_sample_barcode=%%H"
	set "_sample_type=%%I"
	set "_sex_chrom=%%J"
	set "_screen_type=%%K"
	set "_flowcell=%%L"
	set "_class_sx=%%M"
	set "_class_auto=%%N"
	set "_anomaly_description=%%O"
	set "_qc_flag=%%P"
	set "_qc_reason=%%Q"
	set "_ff=%%R"
	set /A inx=0
	if !_batch_name!==batch_name (
	    echo %%G,%%H,%%I,%%J,%%K,%%L,%%M,%%N,%%O,%%P,%%Q,%%R,"result","summary result","auto answer"
	) else (
		set /A idx+=1
		echo 1>&2 sample !idx! processed
	    if "!_anomaly_description!" EQU "NO ANOMALY DETECTED" (
			set "genereal_result=NIPT-analysen viste ingen avvik."
			set "magic_svar=Ja"
			if "!_class_sx!" EQU "NO ANOMALY DETECTED - XY" (
				set "summary_result=NIPT-analysen viste ingen avvik. Fostrets kjønn er gutt."
			) else if "!_class_sx!" EQU "NO ANOMALY DETECTED - XX" (
				set "summary_result=NIPT-analysen viste ingen avvik. Fostrets kjønn er jente."
			) else if "!_class_sx!" EQU "NO ANOMALY DETECTED" (
				set "summary_result=NIPT-analysen viste ingen avvik. Fostrets kjønn ble ikke testet."
			) else if "!_class_sx!" EQU "NOT REPORTABLE" (
				set "summary_result=NIPT-analysen viste ingen avvik. Fosterets kjønn kan ikke bestemmes."
			) else if "!_class_sx!" EQU "NO CHR Y PRESENT" (
				set "summary_result=NIPT-analysen viste ingen avvik. Tvillinggraviditet uten Y-kromosom oppdaget."
			) else if "!_class_sx!" EQU "CHR Y PRESENT" (
				set "summary_result=NIPT-analysen viste ingen avvik. Tvillinggraviditet med Y-kromosom oppdaget."
			) else (
				set "summary_result=NIPT-analysen viste ingen avvik. Sex kromosom aneuploidy klassifisering er "!_class_sx!""
			) 
		) else if "!_anomaly_description!" EQU "INVALIDATED" (
			set "genereal_result=NIPT-analysen var inkonklusiv."
			set "summary_result=NIPT-analysen må rekvireres på nytt"
			set "magic_svar=Nei"
		) else if "!_anomaly_description!" EQU "CANCELLED" (
			set "genereal_result=NIPT-analysen var inkonklusiv."
			set "summary_result=NIPT-analysen må rekvireres på nytt"
			set "magic_svar=Nei"
		) else if "!_anomaly_description!" EQU "NA" (
			set "genereal_result=NIPT-analysen var inkonklusiv."
			set "summary_result=NIPT-analysen må rekvireres på nytt"
			set "magic_svar=Nei"
		) else if "!_anomaly_description:~0,9!" EQU "DETECTED:" (
			set "genereal_result=NIPT-analysen tyder på kromosomavvik. "
			set "magic_svar=Nei"
			set "details="
			for %%c in (13 18 21) do (
				set chrom=%%c
				echo !_anomaly_description! | findstr /C:"+!chrom!;" > nul
				if errorlevel 1  (
					echo !_anomaly_description!| findstr /E /C:"+!chrom!" > nul
					IF !ERRORLEVEL! EQU 0 set "details=!details!Fostret har økt risiko for trisomi !chrom!. "
				) else (
					set "details=!details!Fostret har økt risiko for trisomi !chrom!. "
				)
				echo !_anomaly_description! | findstr /C:"-!chrom!;" > nul
				if errorlevel 1  (
					echo !_anomaly_description!| findstr /E /C:"-!chrom!" > nul
					IF !ERRORLEVEL! EQU 0 set "details=!details!Fostret har økt risiko for monosomi !chrom!. "
				) else (
					set "details=!details!Fostret har økt risiko for monosomi !chrom!. "
				)

				echo !_anomaly_description! | findstr /R /C:"dup(!chrom!)([pq][0-9]" > nul
				IF !ERRORLEVEL! EQU 0 set "details=!details!Delvis duplisering av kromosom !chrom!. "
				echo !_anomaly_description! | findstr /R /C:"del(!chrom!)([pq][0-9]" > nul
				IF !ERRORLEVEL! EQU 0 set "details=!details!Delvis sletting av kromosom !chrom!. "
			)
			for %%d in (XO XXX XXY XYY) do (
				set "sexanom=%%d"
				echo !_anomaly_description! | findstr /C:"!sexanom!;" > nul
				if errorlevel 1  (
					echo !_anomaly_description!| findstr /E /C:"!sexanom!" > nul
					IF !ERRORLEVEL! EQU 0 set "details=!details!Kjønnskromosom er !sexanom!."
				) else (
					set "details=!details!Kjønnskromosom er !sexanom!."
				)
			)
			set "summary_result=NIPT-analysen tyder på kromosomavvik. !details!"
		)
		echo %%G,%%H,%%I,%%J,%%K,%%L,%%M,%%N,%%O,%%P,%%Q,%%R,!genereal_result!,!summary_result!,!magic_svar!
	)	
) 
) >> "%output_file%"

REM encoding UTF8-8-BOM
if exist "%output_file%" powershell -c "$MyFile = Get-Content -Encoding UTF8 '%output_file%'; $MyFile | Out-File -Encoding "UTF8" '%output_file%'"
goto :EOF


: end Batch portion / begin PowerShell hybrid chimera #>

Add-Type -AssemblyName System.Windows.Forms
$f = new-object Windows.Forms.OpenFileDialog
$f.InitialDirectory = "M:\parser"
$f.Title = "Choose NIPT report file(s)"
$f.Filter = "Tab-seperated Values (*.tab)|*.tab|(Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
$f.ShowHelp = $true
$f.Multiselect = $true
[void]$f.ShowDialog()
if ($f.Multiselect) { $f.FileNames } else { $f.FileName }
