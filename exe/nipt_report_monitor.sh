#!/bin/bash

set -euf -o pipefail

DIR="$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)"

export PYTHONPATH="${DIR}/../src":${PYTHONPATH=}

CMD="python3 ${DIR}/../src/swl_integration/interpret_nipt_report.py $*"

# CMD="python ${DIR}/../src/swl_integration/interpret_nipt_report.py --veriseq-output-dir ${DIR}/../testdata/veriseq_output --interpreted-output-dir ${DIR}/../testdata/swl_report --report-match .*_nipt_report_.*.tab$ --processed-reports-file ${DIR}/../testdata/processed_nipt_reports.txt"

# CMD="python ${DIR}/../src/swl_integration/interpret_nipt_report.py --veriseq-output-dir ../testdata/veriseq_output --interpreted-output-dir ../testdata/swl_report --report-match .*_nipt_report_.*.tab$ --processed-reports-file ../testdata/processed_nipt_reports.txt"

$CMD
